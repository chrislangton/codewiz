ALTER TABLE `chrisdla_codewiz`.`articles` 
  ADD COLUMN `show_prev_img` TINYINT(1) NOT NULL DEFAULT '0' AFTER `prev_img`,
  ADD COLUMN `banner_img` VARCHAR(255) NULL AFTER `show_prev_img`,
  ADD COLUMN `rt_markup` TEXT NULL;
