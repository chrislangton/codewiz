delimiter $$

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `disqus_article_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `disqus_id` int(11) DEFAULT NULL,
  `in_reply_to` int(11) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `content` text,
  `likes` int(11) DEFAULT '0',
  `dislikes` int(11) DEFAULT '0',
  `stars` int(11) DEFAULT '0',
  `author_name` varchar(255) NOT NULL,
  `author_disqus_profile` varchar(255) DEFAULT NULL,
  `author_disqus_reputation` float DEFAULT NULL,
  `author_disqus_id` int(11) DEFAULT NULL,
  `author_url` varchar(255) DEFAULT NULL,
  `author_about` varchar(255) DEFAULT NULL,
  `author_avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `disqus_id_UNIQUE` (`disqus_id`),
  KEY `important` (`article_id`,`approved`,`in_reply_to`) USING BTREE,
  KEY `author` (`author_name`,`author_url`,`author_about`,`author_avatar`) USING BTREE,
  KEY `content` (`content`(255)),
  CONSTRAINT `article_id` FOREIGN KEY (`id`) REFERENCES `articles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8$$

