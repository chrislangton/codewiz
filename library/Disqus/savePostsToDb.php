<?php
class savePostsToDb
{
  protected $db;
  private $disqus;
  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    $this->disqus = new Disqus();
    return $this;
  }
  public function runAll( $numProcess = null )
  {
    $articles = $this->db->fetchAssoc( "SELECT id FROM `articles` WHERE approved = ? ORDER BY id DESC" , 1 );
    $disqus = $result = $error = $ins = array(); 
    $inserted=$i=0;
    foreach ( $articles as $article ) {
        $i++;
        $disqus[$article['id']] = $this->disqus->fetchThreads( $article['id'] );
        if ( is_null( $disqus[$article['id']]->response ) || is_string( $disqus[$article['id']]->response ) ) {
            $error[$article['id']]['response'] = $disqus[$article['id']]->response;
        } else {
            $result[$article['id']]['article_id'] = $article['id'];
            $result[$article['id']]['numComments'] = $disqus[$article['id']]->numComments();
            $result[$article['id']]['disqusId'] = $disqus[$article['id']]->disqusId();
            $result[$article['id']]['disqusLink'] = $disqus[$article['id']]->disqusLink();
            $result[$article['id']]['likes'] = $disqus[$article['id']]->likes();
            $result[$article['id']]['approved'] = $disqus[$article['id']]->isClosed() ? 0: 1;
            if ( $result[$article['id']]['numComments'] > 0 ) {
                $result[$article['id']]['comments'] = $disqus[$article['id']]->fetchPosts()->comments();
                foreach ( $result[$article['id']]['comments'] as $comment ) {
                    $count = $this->db->fetchRow( "SELECT count(disqus_id) as found FROM comments WHERE disqus_id = ?" , $comment['disqus_id'] );
                    if ( $count->found == 0 ) {
                        $ins = array(
                            "article_id" => $article['id'],
                            "disqus_article_id" => $comment['disqus_article_id'],
                            "type" => $comment['type'],
                            "disqus_id" => $comment['disqus_id'],
                            "in_reply_to" => $comment['in_reply_to'],
                            "approved" => $comment['approved'],
                            "content" => $comment['content'],
                            "likes" => $comment['likes'],
                            "dislikes" => $comment['dislikes'],
                            "stars" => $comment['stars'],
                            "author_name" => $comment['author_name'],
                            "author_disqus_profile" => $comment['author_disqus_profile'],
                            "author_disqus_reputation" => $comment['author_disqus_reputation'],
                            "author_disqus_id" => $comment['author_disqus_id'],
                            "author_url" => $comment['author_url'],
                            "author_about" => $comment['author_about'],
                            "author_avatar" => $comment['author_avatar'],
                            "datetime" => $comment['datetime']
                        );
                        if ( $this->db->insert( 'comments' , $ins ) ) { $inserted++; }
                    }
                }
            }
            if ( is_numeric( $numProcess ) && (int)$i === (int)$numProcess ) { break; } else { (int)$i++; }
        }
    }
    return $inserted;
  }
  public function runLatest()
  {
    $inserted=0;
    $last = $this->db->fetchRow( "SELECT max(article_id) as article_id FROM comments" );
    $articles = $this->db->fetchAssoc( "SELECT id FROM `articles` WHERE approved = ? AND id > ? ORDER BY id DESC" , array( 1 , $last->article_id ) );
    $disqus = $result = $error = $ins = array(); 
    foreach ( $articles as $article ) {
        $disqus[$article['id']] = $this->disqus->fetchThreads( $article['id'] );
        if ( is_null( $disqus[$article['id']]->response ) || is_string( $disqus[$article['id']]->response ) ) {
            $error[$article['id']]['response'] = $disqus[$article['id']]->response;
        } else {
            $result[$article['id']]['article_id'] = $article['id'];
            $result[$article['id']]['numComments'] = $disqus[$article['id']]->numComments();
            $result[$article['id']]['disqusId'] = $disqus[$article['id']]->disqusId();
            $result[$article['id']]['disqusLink'] = $disqus[$article['id']]->disqusLink();
            $result[$article['id']]['likes'] = $disqus[$article['id']]->likes();
            $result[$article['id']]['approved'] = $disqus[$article['id']]->isClosed() ? 0: 1;
            if ( $result[$article['id']]['numComments'] > 0 ) {
                $result[$article['id']]['comments'] = $disqus[$article['id']]->fetchPosts()->comments();
                foreach ( $result[$article['id']]['comments'] as $comment ) {
                    $count = $this->db->fetchRow( "SELECT count(disqus_id) as found FROM comments WHERE disqus_id = ?" , $comment['disqus_id'] );
                    if ( $count->found == 0 ) {
                        $ins = array(
                            "article_id" => $article['id'],
                            "disqus_article_id" => $comment['disqus_article_id'],
                            "type" => $comment['type'],
                            "disqus_id" => $comment['disqus_id'],
                            "in_reply_to" => $comment['in_reply_to'],
                            "approved" => $comment['approved'],
                            "content" => $comment['content'],
                            "likes" => $comment['likes'],
                            "dislikes" => $comment['dislikes'],
                            "stars" => $comment['stars'],
                            "author_name" => $comment['author_name'],
                            "author_disqus_profile" => $comment['author_disqus_profile'],
                            "author_disqus_reputation" => $comment['author_disqus_reputation'],
                            "author_disqus_id" => $comment['author_disqus_id'],
                            "author_url" => $comment['author_url'],
                            "author_about" => $comment['author_about'],
                            "author_avatar" => $comment['author_avatar'],
                            "datetime" => $comment['datetime']
                        );
                        if ( $this->db->insert( 'comments' , $ins ) ) { $inserted++; }
                    }
                }
            }
        }
    }
    return $inserted;
  }
  public function runNew()
  {
      $comment = $this->db->fetchRow( "SELECT datetime FROM `comments` ORDER BY datetime ASC LIMIT 1" );
      $inserted=0;
      $posts = $this->disqus->fetchPostSince( $comment->datetime )->comments();
      if ( count( $posts ) > 0 ) {
        foreach ( $posts as $comment ) {
            $upd = array(
                "disqus_article_id" => $comment['disqus_article_id'],
                "in_reply_to" => $comment['in_reply_to'],
                "approved" => $comment['approved'],
                "content" => $comment['content'],
                "likes" => $comment['likes'],
                "dislikes" => $comment['dislikes'],
                "stars" => $comment['stars'],
                "author_name" => $comment['author_name'],
                "author_disqus_profile" => $comment['author_disqus_profile'],
                "author_disqus_reputation" => $comment['author_disqus_reputation'],
                "author_url" => $comment['author_url'],
                "author_about" => $comment['author_about'],
                "author_avatar" => $comment['author_avatar'],
                "datetime" => $comment['datetime']
            );
            if ( $this->db->insert( 'comments' , $upd ) ) { $inserted++; }
        }
      }
      return $inserted;
  }
  public function runUpdate()
  {
    $comments = $this->db->fetchAssoc( "SELECT disqus_id FROM `comments`" );
    $updated=0;
    foreach ( $comments as $comment )
    {
      $post = $this->disqus->fetchOnePost( (int)$comment['disqus_id'] )->comments();
      if ( is_array($post) && 0 != $comment['disqus_id'] && count( $post[$comment['disqus_id']] ) > 0 ) {
        $upd = array(
            "disqus_article_id" => $post[$comment['disqus_id']]['disqus_article_id'],
            "in_reply_to" => $post[$comment['disqus_id']]['in_reply_to'],
            "approved" => $post[$comment['disqus_id']]['approved'],
            "content" => $post[$comment['disqus_id']]['content'],
            "likes" => $post[$comment['disqus_id']]['likes'],
            "dislikes" => $post[$comment['disqus_id']]['dislikes'],
            "stars" => $post[$comment['disqus_id']]['stars'],
            "author_name" => $post[$comment['disqus_id']]['author_name'],
            "author_disqus_profile" => $post[$comment['disqus_id']]['author_disqus_profile'],
            "author_disqus_reputation" => $post[$comment['disqus_id']]['author_disqus_reputation'],
            "author_url" => $post[$comment['disqus_id']]['author_url'],
            "author_about" => $post[$comment['disqus_id']]['author_about'],
            "author_avatar" => $post[$comment['disqus_id']]['author_avatar'],
            "datetime" => $post[$comment['disqus_id']]['datetime']
        );
        if ( $this->db->update( 'comments' , $upd , "disqus_id =".$comment['disqus_id'] ) ) { $updated++; }
      }
    }
    return $updated;
  }
}
?>