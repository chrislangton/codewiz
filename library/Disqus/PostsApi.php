<?php
/**
 * Description of Disqus
 *
 * @author CLangton
 */
class Disqus
{
	public $response = null;
        public $posts = null;
        protected $_conf;
        private $thread;

        public function __construct()
	{
            $this->_conf = Codewiz_Bootstrap::getInstance()->getConf();
            return $this;
	}
        public function fetchThreads( $thread = false )
        {
            if ( $thread )
            {
                $this->thread = $thread;
                // construct the query with our apikey and the query we want to make
                // Change api_key to api_secret when using your secret key
                /*
                        DIFFERENT TYPES OF THREAD LOOKUPS:
                        1. By DISQUS thread ID (default): thread=%s נthread IDs are universally unique in DISQUS, so you can remove 'forum' param if you like
                        2. By identifier: thread:ident=%s נrequires the forum parameter
                        3. By URL: thread:link=%s נrequires the forum parameter
                 */
                $endpoint = 'http://disqus.com/api/3.0/threads/details.json?api_key='.urlencode( $this->_conf->api->disqus->apikey ).'&forum='.$this->_conf->api->disqus->forum.'&thread:ident='.urlencode($thread);
                // setup curl to make a call to the endpoint
                $session = curl_init($endpoint);
                // indicates that we want the response back rather than just returning a "TRUE" string
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                // execute GET and get the session back
                $result = curl_exec($session);
                // close connection
                curl_close($session);
                $result_json = json_decode($result,true);
                $this->response = $result_json['response'];
            }
            return $this;
        }
        public function fetchPosts()
        {
            if ( is_null( $this->response ) || is_string( $this->response ) ) { return $this; }
            $endpoint = 'http://disqus.com/api/3.0/threads/listPosts.json?api_key='.urlencode( $this->_conf->api->disqus->apikey ).'&forum='.$this->_conf->api->disqus->forum.'&thread:ident='.urlencode( $this->thread );
            // setup curl to make a call to the endpoint
            $session = curl_init($endpoint);
            // indicates that we want the response back rather than just returning a "TRUE" string
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // execute GET and get the session back
            $result = curl_exec($session);
            // close connection
            curl_close($session);
            $result_json = json_decode($result,true);
            $this->posts = $result_json['response'];
            return $this;
        }
        public function fetchOnePost( $id )
        {
            if ( !is_numeric( $id ) ) { return $this; }
            $endpoint = 'http://disqus.com/api/3.0/posts/details.json?api_key='.urlencode( $this->_conf->api->disqus->apikey ).'&post='.urlencode( (int)$id );
            // setup curl to make a call to the endpoint
            $session = curl_init($endpoint);
            // indicates that we want the response back rather than just returning a "TRUE" string
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // execute GET and get the session back
            $result = curl_exec($session);
            // close connection
            curl_close($session);
            $result_json = json_decode($result,true);
            $this->posts = $result_json['response'];
            return $this;
        }
        public function fetchPostSince( $since = null )
        {
            if ( is_null( $since ) ) { return $this; }
            $endpoint = 'http://disqus.com/api/3.0/posts/list.json?api_key='.urlencode( $this->_conf->api->disqus->apikey ).'&forum='.$this->_conf->api->disqus->forum.'&since='.urlencode( strtotime( $since ) );
            // setup curl to make a call to the endpoint
            $session = curl_init($endpoint);
            // indicates that we want the response back rather than just returning a "TRUE" string
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // execute GET and get the session back
            $result = curl_exec($session);
            // close connection
            curl_close($session);
            $result_json = json_decode($result,true);
            $this->posts = $result_json['response'];
            return $this;
        }
        public function comments()
	{
            if ( is_null( $this->posts ) || is_string( $this->posts ) ) { return $this; }
            $comment = array();
            if ( isset( $this->posts['forum'] ) && $this->posts['forum'] == $this->_conf->api->disqus->forum ) { $temp = $this->posts; $this->posts = array(); $this->posts[0] = $temp; unset($temp); }
            foreach ( $this->posts as $post )
            {
                $comment[$post['id']]['type'] = is_null( $post['parent'] ) ? "new" : "reply";
                $comment[$post['id']]['disqus_id'] = $post['id'];
                $comment[$post['id']]['in_reply_to'] = $post['parent'];
                $comment[$post['id']]['approved'] = $post['isApproved'];
                $comment[$post['id']]['content'] = $post['raw_message'];
                $comment[$post['id']]['datetime'] = str_replace( "T" , " " , $post['createdAt'] );
                $comment[$post['id']]['likes'] = $post['likes'];
                $comment[$post['id']]['dislikes'] = $post['dislikes'];
                $comment[$post['id']]['disqus_article_id'] = $post['thread'];
                $comment[$post['id']]['stars'] = $post['points'];
                $comment[$post['id']]['author_name'] = $post['author']['name'];
                $comment[$post['id']]['author_disqus_profile'] = $post['author']['profileUrl'];
                $comment[$post['id']]['author_disqus_reputation'] = $post['author']['reputation'];
                $comment[$post['id']]['author_disqus_id'] = $post['author']['id'];
                $comment[$post['id']]['author_url'] = $post['author']['url'];
                $comment[$post['id']]['author_about'] = $post['author']['about'] == '' ? $this->_conf->appName." reader. ".($post['author']['url']||"") : $post['author']['about'];
                $comment[$post['id']]['author_avatar'] = $post['author']['avatar']['permalink'];
            }
            return $comment;
	}
        public function numComments()
	{
		return $this->response['posts'];
	}
	public function title()
	{
		return $this->response['title'];
	}
	public function disqusId()
	{
		return $this->response['id'];
	}
	public function disqusLink()
	{
		return $this->response['link'];
	}
	public function reactions()
	{
		return $this->response['reactions'];
	}
	public function likes()
	{
		return $this->response['likes'];
	}
	public function dislikes()
	{
		return $this->response['dislikes'];
	}
	public function userScore()
	{
		return $this->response['userScore'];
	}
	public function category()
	{
		return $this->response['category'];
	}
	public function createdAt()
	{
		return $this->response['createdAt'];
	}
	public function isClosed()
	{
		return $this->response['isClosed'];
	}
	public function isDeleted()
	{
		return $this->response['isDeleted'];
	}
}	
