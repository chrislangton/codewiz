<?php
/**
 * Interface for bootstrap classes
 *
 * @category   Codewiz
 * @copyright  Copyright (c) 2013 Christopher Langton. (http://chrisdlangton.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
interface Bootstrap_Interface
{
    /**
     * Constructor
     *
     * @return Class Self
     */
    public function __construct();
    /**
     * Retrieve Bootstrap Instance
     *
     * @return Class Self
     */
    public static function getInstance();
    /**
     * Run the application
     *
     * @return Class Self
     */
    public function run();
    /**
     * Retrieve ZF application.ini
     *
     * @return Zend_Config
     */
    public function getConf();    
}
