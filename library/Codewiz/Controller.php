<?php

class Codewiz_Controller extends Zend_Controller_Action
{
    protected $_conf;
    protected $_em;
    protected $_session;
    
    public function __call($method, $args)
    {
      if ('Action' == substr($method, -6)) {
          // If the action method was not found, forward to the
          // index action
          return $this->_forward('index');
      }
      // all other methods throw an exception
      throw new Exception('Invalid method "'
                          . $method
                          . '" called',
                          500);
    }
    public function getSession()
    {
        if ( !is_object( $this->_session ) ) $this->_session = Zend_Registry::get( 'session' );
	return $this->_session;
    }
    public function getEm()
    {
        if ( !is_object( $this->_em ) ) $this->_em = Zend_Registry::get( 'entity_manager' );
	return $this->_em;
    }
    public function getConf()
    {
        if ( !is_object( $this->_conf ) ) $this->_conf = Codewiz_Bootstrap::getInstance()->getConf();
	return $this->_conf;
    }
    public function raiseAlert( $txt , $type = 'info' , $block = false )
    {
        $allowed_types = array( "success" , "error" , "warning" , "info" );
        if ( in_array( $type , $allowed_types ) )
        return is_string($txt) ? $this->view->assign( 'alert' , (object)array( "txt"=>$txt , "type"=>$type , "block" => $block ) ) : null;
        else return;
    }
    public function setMeta()
    {
        $this->view->assign( array(
            'encoding' => $this->getConf()->encoding,
            'version' => $this->getConf()->version,
            'author' => $this->getConf()->author,
            'GoogleAccount' => $this->getConf()->api->google->account,
            'GoogleAnalytics'=> $this->getConf()->api->google->analytics,
            'GoogleVerification' => $this->getConf()->api->google->verification,
            'js' => $this->getConf()->js,
            'css' => $this->getConf()->css,
            'headerDir' => $this->getConf()->headerDir,
            'footerDir' => $this->getConf()->footerDir,
            'title' => $this->getConf()->appName,
            'description' => $this->getConf()->description,
            'abstract' => $this->getConf()->shortDescription,
            'image_src' => $this->getConf()->logoSmall,
            'adminEmail' => $this->getConf()->adminEmail,
            'keywords' => $this->getConf()->keywords,
            'addthis_pubid' => $this->getConf()->api->addthis->pubid,
            'fb_app_id' => $this->getConf()->api->facebook->app_id,
            'fb_admins' => $this->getConf()->api->facebook->admins,
            'tw_site' => $this->getConf()->api->twitter->site_profile,
            'tw_author' => $this->getConf()->api->twitter->content_author
        ) );
        return $this;
    }
    public static function get_user_agent_string() {
      if (isset($_SERVER['HTTP_USER_AGENT']))
          $return = strtolower($_SERVER['HTTP_USER_AGENT']);
      else
          $return = '';
      return $return;
    }
    public static function curl_get_file_headers( $url ) {
        $matches = 'mixed';
        $result = (object)array(
            "status" => 500,        // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            "content_length" => 0,
            "content_type" => "unknown",
            "last_modified" => new \DateTime()
        );
        $curl = curl_init( $url );
        // Issue a HEAD request and follow any redirects.
        curl_setopt( $curl, CURLOPT_NOBODY, true );
        curl_setopt( $curl, CURLOPT_HEADER, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_MAXREDIRS, 10 ); //follow up to 10 redirections - avoids loops
        curl_setopt( $curl, CURLOPT_USERAGENT, static::get_user_agent_string() );
        $data = curl_exec( $curl );

        if( $data ) {
          if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
            $result->status = (int)$matches[1];
          }
          if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
            $content_length = (int)$matches[1];
          }
          if( preg_match( "#Content-Type:\s([^\s]*)#is", $data, $matches ) ) {
            $content_type = $matches[1];
          }
          if( preg_match( "#Last-Modified:\s([a-z]{3},\s[0-9]{1,2}\s[a-z]{3}\s[0-9]{4}\s[0-9]{2}:[0-9]{2}:[0-9]{2})#is", $data, $matches ) ) {
            $last_modified = $matches[1];
          }

          if( $result->status == 200 || ($result->status > 300 && $result->status <= 308) ) {
              if ( !$content_length > 0 ) {
                  $content_length = curl_getinfo( $curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
              }
              $result->content_length = $content_length;
              $result->content_type = $content_type;
              $result->last_modified = $last_modified;
          }
        }
        curl_close( $curl );
        return $result;
    }
    public static function trim_words($str, $length, $strip_html = false, $ellipses = false, $eol = false) 
    {
      //strip tags, if desired
      if ($strip_html) {
              $str = strip_tags($str);
      }
      //no need to trim, already shorter than trim length
      if (strlen($str) <= $length) {
              return $str;
      }
      //find last space within length
      $last_space = strrpos(substr($str, 0, $length), ' ');
      $trimmed_text = substr($str, 0, $last_space);
      //add ellipses (...)
      if ($ellipses) {
              $trimmed_text .= '...';
      }
      //add end of line
      if ($eol) {
              $trimmed_text .= PHP_EOL;
      }
      return $trimmed_text;
    }
    public static function xmlentities( $string ) {
              $string = preg_replace('/[^\00-\255]+/u', '', $string);
              return str_replace(array("&", "<", ">", "\"", "'"),
      array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), $string);
    }
    /* Method   : uniqueSalt()
     * Purpose  : create a random salt for the password hash
     * Return   : sha1 salt
     */
    private static function uniqueSalt()
    {
        return substr( sha1( mt_rand() ) , 0 , 22 );
    }
    /* Method   : hashpwd($password)
     * Purpose  : create a hash for the password
     * @prop    : $password to be hashed
     * Return   : hashed password
     */
    private static function hashpwd( $password )
    {
        return crypt( $password,
                    self::$algo .
                    self::$cost .
                    '$' . self::uniqueSalt() );
    }
    /* Method   : checkPassword($hash, $password)
     * Purpose  : checks user password with database stored password for user
     * @prop    : $hash database stored password for user
     * @prop    : $password entered by user to be checked
     * Return   : true/false
     */
    private static function checkPassword( $hash , $password ) {
        $full_salt = substr( $hash , 0 , 29 );
        $new_hash = crypt( $password , $full_salt );
        return ( $hash == $new_hash );
    }
    public function verifyLogin( $request , $user )
    {
        $result = (object)array( "auth" => false , "err" => "" );
        if ( !isset( $request->email ) || !isset( $request->password ) )
        {
            $result->err .= 'Both User Email and Password are required'.PHP_EOL;
        } 
        else
        {
            if (strlen($request->email) < 6) { $result->err .= 'User Email must be at least 6 characters'.PHP_EOL; }
            if (strlen($request->password) < 8) { $result->err .= 'Password must be at least 8 characters'.PHP_EOL; }
            if (strlen($request->email) > 254) { $result->err .= 'User Email must be less than 255 characters'.PHP_EOL; }
            if (strlen($request->password) > 254) { $result->err .= 'Password must be less than 255 characters'.PHP_EOL; }
        }
        if ( $user->active !='1' )
        {
                $result->err .= 'User is not active'.PHP_EOL;
        }
        if ( static::checkPassword( $user->password , $request->password ) )
        {
            $this->getSession()->auth = true;
            $this->getSession()->email = $user->email; 
            $this->getSession()->userId = $user->id;
            $this->getSession()->url = $user->url;
            $this->getSession()->avatar = $user->avatar_url;
            $this->getSession()->userName = $user->fname . ' ' . $user->lname;

            $result->auth = true;
        }
        else
        {
                $result->err .= 'Incorrect Password provided for email '.$request->email.PHP_EOL;
        }
        return $result;
    }
}
?>
