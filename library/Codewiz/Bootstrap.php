<?php
require_once 'Interfaces/Bootstrap_Interface.php';
/**
 * Bootstrap single access file Class
 *
 * @author Chris Langton
 */
class Codewiz_Bootstrap implements Bootstrap_Interface
{
    protected static $_instance = null;
    protected $_config;
    /**
     * 
     * @return Codewiz_Bootstrap 
     */
    public static function getInstance()
    {
        if ( null === self::$_instance ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function __construct() {
        return $this;
    }

    /**
     * 
     * @return Codewiz_Bootstrap 
     */
    public function run()
    {
        $this->_setupAutoloader()
                ->_initEnv()
                ->_initLogger()
                ->_startSession()
                ->_initDatabase()
                ->_setupDoctrine()
                ->_prepareApis()
                ->_initZendFrontController();
        return $this;
    }
    /**
     *
     * @return Zend_Config
     */
    public function getConf()
    {
        if ( !is_object( $this->_config ) ) {
            $this->_config = new Zend_Config_Ini( APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV );
        }
        return $this->_config;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _setupAutoloader()
    {
        require_once 'Zend/Loader/Autoloader.php';
        Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
                    'basePath' => APPLICATION_PATH,
                    'namespace' => 'Application',
                ));

        $resourceLoader->addResourceType('model', 'models/', 'Model');

	$this->_resourceLoader = $resourceLoader;
        return $this;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _initLogger()
    {
        // setup auto-magical handlers for this instance
        $logger = new Codewiz_Logger( array(
            "enableHandler" => array(
                "error"     => true,
                "exception" => true,
                "fatal"     => true
            )
        ) , true );
        return $this;        
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _initEnv()
    {
        date_default_timezone_set( $this->getConf()->default_timezone );
        mb_internal_encoding( $this->getConf()->encoding );
        return $this;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _initDatabase()
    {
        try {
            $dbAdapter = Zend_Db::factory( $this->getConf()->resources->db->adapter, $this->getConf()->resources->db->params->toArray() );
            $dbAdapter->getConnection();
            //Zend_Db_Table_Abstract::setDefaultAdapter( $dbAdapter );
        } catch (Zend_Db_Adapter_Exception $e) {
            echo '<pre>';
            var_dump( $e );
            echo '</pre>';            
        } catch (Zend_Exception $e) {
            echo '<pre>';
            var_dump( $e );
            echo '</pre>';
        }
        $dbAdapter->setFetchMode(Zend_Db::FETCH_OBJ);
	Zend_Registry::set( 'db', $dbAdapter );
        return $this;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _startSession()
    {
        $sessionAdmin = new Zend_Session_Namespace( 'session' );
        if ( ( isset( $this->getConf()->session->enableTimeout ) && $this->getConf()->session->enableTimeout ) && ( isset( $this->getConf()->session->timeout ) && is_numeric( $this->getConf()->session->timeout ) ) ) { 
            $sessionAdmin->setExpirationSeconds( $this->getConf()->adminSessionTimeout );
        }
        Zend_Registry::set( 'session' , $sessionAdmin );
        !isset( $sessionAdmin->pageView ) ? $sessionAdmin->pageView = 1 : $sessionAdmin->pageView++ ;
        return $this;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _prepareApis()
    {
        require_once 'Disqus/PostsApi.php';
        defined('DISQUS_FORUM')
            || define('DISQUS_FORUM', $this->getConf()->api->disqus->forum );
        return $this;
    }
    /**
     *
     * @return Codewiz_Bootstrap 
     */
    protected function _initZendFrontController()
    {
        $ZendFrontController = Zend_Controller_Front::getInstance();
        if (APPLICATION_ENV === 'development') {
            $ZendFrontController->setParam('noErrorHandler', false)
                    ->throwExceptions(true);
        }
        $ZendFrontController->run('../application/controllers');
        return $this;
    }
    protected function _setupDoctrine() 
    {
	$autoloader = Zend_Loader_Autoloader::getInstance();
	// Define the connection parameters
	$connectOptions = array(
	    'default' => array(
		'driver'   => strtolower($this->getConf()->resources->db->adapter),
		'host'     => $this->getConf()->resources->db->params->host,
		'dbname'   => $this->getConf()->resources->db->params->dbname,
		'user'     => $this->getConf()->resources->db->params->username,
		'password' => $this->getConf()->resources->db->params->password
	    )
	);
	$config	    = $this->getConf()->resources->entityManager;	
	$modelPath  = array();
	$modules    = $this->getConf()->resources->modules;
	
	foreach ($modules as $module => $moduleName) {
	    
	    $moduleModelDirectory = APPLICATION_PATH . '/modules/' . $module . $config->connection->moduleModelDirectory;
	    
	    $classLoader = new \Doctrine\Common\ClassLoader($moduleName . 'Entities', $moduleModelDirectory, 'loadClass');
	    $autoloader->pushAutoloader(array($classLoader, 'loadClass'), $moduleName . 'Entities');
	    
	    $classLoader = new \Doctrine\Common\ClassLoader($moduleName . 'Repositories', $moduleModelDirectory, 'loadClass');
	    $autoloader->pushAutoloader(array($classLoader, 'loadClass'), $moduleName . 'Repositories');
	    
	    $modelPath[] = $moduleModelDirectory;
	}
	
	$configEm = new \Doctrine\ORM\Configuration;
	
//	if (APPLICATION_ENV !== 'development') {
//	    $cache = new \Doctrine\Common\Cache\ApcCache;
//	} else {
//	    $cache = new \Doctrine\Common\Cache\ArrayCache;
//	}
        $cache = new \Doctrine\Common\Cache\ArrayCache;
	$driverImpl = $configEm->newDefaultAnnotationDriver($modelPath);
	$configEm->setMetadataCacheImpl($cache);
	$configEm->setMetadataDriverImpl($driverImpl);
	//// Configure proxies
	$configEm->setAutoGenerateProxyClasses($config->connection->proxies->generate);      
	$configEm->setProxyNamespace($config->connection->proxies->ns);
	$configEm->setProxyDir(APPLICATION_PATH . '/models/'.$config->connection->proxies->ns);
	$configEm->setQueryCacheImpl($cache);
        
	$em = \Doctrine\ORM\EntityManager::create($connectOptions['default'], $configEm);
	Zend_Registry::set('entity_manager', $em);
	
	return $this;
    }
}