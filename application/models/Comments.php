<?php
/**
 * Description of comments
 *
 * @author CLangton
 */
class Application_Model_Comments
{
  protected $db;

  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    return $this;
  }
  public function getByArticle( $article_id )
  {
    return $this->db->fetchAssoc( "SELECT c.*, a.`article_title` FROM `comments` c 
                                LEFT JOIN `articles` a ON c.`article_id` = a.`id`
                                WHERE c.approved = ? AND c.article_id = ? ORDER BY c.datetime DESC" , array( 1 , $article_id ) );
  }
  public function getAll()
  {
    return $this->db->fetchAssoc( "SELECT c.*, a.`article_title` FROM `comments` c 
                                LEFT JOIN `articles` a ON c.`article_id` = a.`id`
                                WHERE c.approved = ? ORDER BY c.datetime DESC" , 1 );
  }
  public function getLatest( $limit = 5 , $exclude = null )
  {
    return is_null( $exclude ) ? 
      $this->db->fetchAssoc( "SELECT c.*, a.`article_title` FROM `comments` c
                LEFT JOIN `articles` a ON c.`article_id` = a.`id`
                WHERE c.`approved` = ? ORDER BY c.`datetime` DESC LIMIT " . (int)$limit , 1 )
    :
      $this->db->fetchAssoc( "SELECT c.*, a.`article_title` FROM `comments` c
                LEFT JOIN `articles` a ON c.`article_id` = a.`id`
                WHERE c.`approved` = ? AND c.`article_id` != ? ORDER BY c.`datetime` DESC LIMIT " . (int)$limit , array( 1 , $exclude ) )
    ;
  }
}
