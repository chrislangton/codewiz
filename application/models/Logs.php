<?php
/**
 * Description of articles
 *
 * @author chris
 */
class Application_Model_Logs {
  
  protected $db;

  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    return $this;
  }
  public function getAll( $limit=100, $order="id" , $direction="DESC" )
  {
    return $this->db->fetchAssoc( "SELECT * FROM `logger` ORDER BY ".$order." ".$direction." LIMIT " . (int)$limit ) ;
  }
  public function fetchAll( $limit=100 , $order="id" , $direction="DESC" )
  {
    $direction = strtoupper($direction) === "ASC" ? "ASC" : "DESC" ;
    return $this->db->fetchAssoc( "SELECT * FROM `logger` ORDER BY ".$order." ".$direction." LIMIT " . (int)$limit ) ;
  }
  public function getByType( $type = "debug" , $limit=100, $order="id" , $direction="DESC" )
  {
    if ( is_null( $type ) ) { return array(); }
    return $this->db->fetchAssoc( "SELECT * FROM `logger` WHERE type = ? ORDER BY ".$order." ".$direction." LIMIT " . (int)$limit , array( $type ) ) ;
  }
  public function getById( $id )
  {
    if ( !is_numeric($id) ) return;
    return $this->db->fetchAssoc( "SELECT * FROM `logger` WHERE id = ? LIMIT 1" , array( $id ) ) ;
  }
  public function delRowById( $id )
  {
    if ( !is_numeric($id) ) return;
    $results = (object)array( "result" => false );
    $results->result = $this->db->query( 'DELETE FROM `logger` WHERE id=? LIMIT 1' , array( $id ) ) ? true : false;
    return $results;
  }
}

?>
