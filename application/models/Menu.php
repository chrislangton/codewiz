<?php
/**
 * Description of articles
 *
 * @author chris
 */
class Application_Model_Menu {
  
  protected $db;
  protected $request;
  private $session;

  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    $this->session = Zend_Registry::get( 'session' );
    return $this;
  }
  public function buildAdminSidebar()
  {
    $menu = (object)array();
    $menu->Articles = array(
            "Create New Article" => HOST_URL . "admin/article/",
            "Manage Articles" => HOST_URL . "admin/view-articles/",
            "Manage Categories" => HOST_URL . "admin/edit-categories/"
        );
    $menu->Users = array(
            "Manage Users" => HOST_URL . "admin/view-users/"
        );
    $menu->Management = array(
            "Cron Jobs" => HOST_URL . "admin/cron/",
            "Logs" => HOST_URL . "admin/logs/"
        );
    return $menu;      
  }
  public function buildCronJobs()
  {
    $menu = (object)array();
    $menu->Disqus = array(
            "Get All Comments" => HOST_URL . "cron/disqusall/",
            "Get Comments for Latest Articles" => HOST_URL . "cron/disquslatest/",
            "Get New Comments" => HOST_URL . "cron/disqusnew/",
            "Update All Comments" => HOST_URL . "cron/disqusupdate/",
        );      
    $menu->Sitemap = array(
            "Generate Sitemap XML" => HOST_URL . "cron/generate-sitemap/",
            "Ping Sitemap" => HOST_URL . "cron/ping-sitemap/"
        );      
    $menu->Rss = array(
            "Generate RSS XML" => HOST_URL . "cron/generate-rss/",
            "Ping RSS" => HOST_URL . "cron/ping-rss/"
        );      
    return $menu;      
  }
  public function build()
  {
    $menu = (object)array();
    $menu->main = array(
            "Home" => HOST_URL
        );
    $categories = $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? ORDER BY display ASC" , 1 );
    foreach( $categories as $key => $category )
    {
        $menu->Categories[$category['display']] = HOST_URL . "index/category/" . $category['id'];
    }
    $menu->Topics = array(
            "Tutorials" => HOST_URL . "index/tag/how+to",
            "Raspberry Pi" => HOST_URL . "index/tag/raspberry+pi",
            "Ubuntu" => HOST_URL . "index/tag/ubuntu",
            "PHP" => HOST_URL . "index/tag/php",
            "JavaScript" => HOST_URL . "index/tag/javascript",
            "CSS3" => HOST_URL . "index/tag/css",
            "HTML5" => HOST_URL . "index/tag/html",
            "jQuery" => HOST_URL . "index/tag/jquery",
            "DB/SQL" => HOST_URL . "index/tag/sql"
        );
    if ( isset( $this->session->auth ) && $this->session->auth )
    {
        $menu->Settings = array(
                "Articles" => HOST_URL . "admin/view-articles/",
                "Manage Categories" => HOST_URL . "admin/edit-categories/",
                "Manage Users" => HOST_URL . "admin/view-users/",
                "Cron Jobs" => HOST_URL . "admin/cron/",
                "Logs" => HOST_URL . "admin/logs/"
            );
    }
    return $menu;
  }
}