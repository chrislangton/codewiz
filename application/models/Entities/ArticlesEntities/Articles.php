<?php

namespace ArticlesEntities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticlesEntities\Articles
 *
 * @ORM\Table(name="articles")
 * @ORM\Entity(repositoryClass="ArticlesRepositories\Articles")
 */
class Articles
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $prevImg
     *
     * @ORM\Column(name="prev_img", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prevImg;

    /**
     * @var string $title
     *
     * @ORM\Column(name="article_title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string $meta
     *
     * @ORM\Column(name="meta", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $meta;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $content;

    /**
     * @var datetime $datetime
     *
     * @ORM\Column(name="datetime", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $datetime;

    /**
     * @var string $createdip
     *
     * @ORM\Column(name="createdip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdip;

    /**
     * @var integer $approved
     *
     * @ORM\Column(name="approved", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approved;

    /**
     * @var datetime $dtapproved
     *
     * @ORM\Column(name="dtapproved", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtapproved;

    /**
     * @var string $approvedbyip
     *
     * @ORM\Column(name="approvedbyip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approvedbyip;

    /**
     * @var integer $draft
     *
     * @ORM\Column(name="draft", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $draft;

    /**
     * @var integer $views
     *
     * @ORM\Column(name="views", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $views;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CommentsEntities\Comments", mappedBy="article")
     */
    private $comments;

    /**
     * @var CategoryEntities\Category
     *
     * @ORM\ManyToOne(targetEntity="CategoryEntities\Category", inversedBy="articles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $category;

    /**
     * @var UserEntities\User
     *
     * @ORM\ManyToOne(targetEntity="UserEntities\User", inversedBy="articles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="authorid", referencedColumnName="id", nullable=true)
     * })
     */
    private $author;

    /**
     * @var UserEntities\User
     *
     * @ORM\ManyToOne(targetEntity="UserEntities\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approvedby", referencedColumnName="id", nullable=true)
     * })
     */
    private $approvedby;

    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prevImg
     *
     * @param string $prevImg
     * @return Articles
     */
    public function setPrevImg($prevImg)
    {
        $this->prevImg = $prevImg;
        return $this;
    }

    /**
     * Get prevImg
     *
     * @return string 
     */
    public function getPrevImg()
    {
        return $this->prevImg;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Articles
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set meta
     *
     * @param string $meta
     * @return Articles
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * Get meta
     *
     * @return string 
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Articles
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set datetime
     *
     * @param datetime $datetime
     * @return Articles
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * Get datetime
     *
     * @return datetime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set createdip
     *
     * @param string $createdip
     * @return Articles
     */
    public function setCreatedip($createdip)
    {
        $this->createdip = $createdip;
        return $this;
    }

    /**
     * Get createdip
     *
     * @return string 
     */
    public function getCreatedip()
    {
        return $this->createdip;
    }

    /**
     * Set approved
     *
     * @param integer $approved
     * @return Articles
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
        return $this;
    }

    /**
     * Get approved
     *
     * @return integer 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set dtapproved
     *
     * @param datetime $dtapproved
     * @return Articles
     */
    public function setDtapproved($dtapproved)
    {
        $this->dtapproved = $dtapproved;
        return $this;
    }

    /**
     * Get dtapproved
     *
     * @return datetime 
     */
    public function getDtapproved()
    {
        return $this->dtapproved;
    }

    /**
     * Set approvedbyip
     *
     * @param string $approvedbyip
     * @return Articles
     */
    public function setApprovedbyip($approvedbyip)
    {
        $this->approvedbyip = $approvedbyip;
        return $this;
    }

    /**
     * Get approvedbyip
     *
     * @return string 
     */
    public function getApprovedbyip()
    {
        return $this->approvedbyip;
    }

    /**
     * Set draft
     *
     * @param integer $draft
     * @return Articles
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;
        return $this;
    }

    /**
     * Get draft
     *
     * @return integer 
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Articles
     */
    public function setViews($views)
    {
        $this->views = $views;
        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Add comments
     *
     * @param CommentsEntities\Comments $comments
     * @return Articles
     */
    public function addComment(\CommentsEntities\Comments $comments)
    {
        $this->comments[] = $comments;
        return $this;
    }

    /**
     * Remove comments
     *
     * @param CommentsEntities\Comments $comments
     */
    public function removeComment(\CommentsEntities\Comments $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set category
     *
     * @param CategoryEntities\Category $category
     * @return Articles
     */
    public function setCategory(\CategoryEntities\Category $category = null)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     *
     * @return CategoryEntities\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set author
     *
     * @param UserEntities\User $author
     * @return Articles
     */
    public function setAuthor(\UserEntities\User $author = null)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     *
     * @return UserEntities\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set approvedby
     *
     * @param UserEntities\User $approvedby
     * @return Articles
     */
    public function setApprovedby(\UserEntities\User $approvedby = null)
    {
        $this->approvedby = $approvedby;
        return $this;
    }

    /**
     * Get approvedby
     *
     * @return UserEntities\User 
     */
    public function getApprovedby()
    {
        return $this->approvedby;
    }
}