<?php

namespace UserEntities;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserEntities\Permissions
 *
 * @ORM\Table(name="users_permissions")
 * @ORM\Entity(repositoryClass="UserRepositories\Permissions")
 */
class Permissions
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $isAdmin
     *
     * @ORM\Column(name="admin_user", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAdmin;

    /**
     * @var integer $isAuthor
     *
     * @ORM\Column(name="create_article", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAuthor;

    /**
     * @var integer $apiGet
     *
     * @ORM\Column(name="api_get", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiGet;

    /**
     * @var integer $apiPost
     *
     * @ORM\Column(name="api_post", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiPost;

    /**
     * @var integer $apiDelete
     *
     * @ORM\Column(name="api_delete", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiDelete;

    /**
     * @var integer $apiPut
     *
     * @ORM\Column(name="api_put", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiPut;

    /**
     * @var UserEntities\User
     *
     * @ORM\OneToOne(targetEntity="UserEntities\User", inversedBy="permissions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true, nullable=true)
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAdmin
     *
     * @param integer $isAdmin
     * @return Permissions
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return integer 
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set isAuthor
     *
     * @param integer $isAuthor
     * @return Permissions
     */
    public function setIsAuthor($isAuthor)
    {
        $this->isAuthor = $isAuthor;
        return $this;
    }

    /**
     * Get isAuthor
     *
     * @return integer 
     */
    public function getIsAuthor()
    {
        return $this->isAuthor;
    }

    /**
     * Set apiGet
     *
     * @param integer $apiGet
     * @return Permissions
     */
    public function setApiGet($apiGet)
    {
        $this->apiGet = $apiGet;
        return $this;
    }

    /**
     * Get apiGet
     *
     * @return integer 
     */
    public function getApiGet()
    {
        return $this->apiGet;
    }

    /**
     * Set apiPost
     *
     * @param integer $apiPost
     * @return Permissions
     */
    public function setApiPost($apiPost)
    {
        $this->apiPost = $apiPost;
        return $this;
    }

    /**
     * Get apiPost
     *
     * @return integer 
     */
    public function getApiPost()
    {
        return $this->apiPost;
    }

    /**
     * Set apiDelete
     *
     * @param integer $apiDelete
     * @return Permissions
     */
    public function setApiDelete($apiDelete)
    {
        $this->apiDelete = $apiDelete;
        return $this;
    }

    /**
     * Get apiDelete
     *
     * @return integer 
     */
    public function getApiDelete()
    {
        return $this->apiDelete;
    }

    /**
     * Set apiPut
     *
     * @param integer $apiPut
     * @return Permissions
     */
    public function setApiPut($apiPut)
    {
        $this->apiPut = $apiPut;
        return $this;
    }

    /**
     * Get apiPut
     *
     * @return integer 
     */
    public function getApiPut()
    {
        return $this->apiPut;
    }

    /**
     * Set user
     *
     * @param UserEntities\User $user
     * @return Permissions
     */
    public function setUser(\UserEntities\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return UserEntities\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}