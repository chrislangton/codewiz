<?php

namespace UserEntities;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserEntities\User
 *
 * @ORM\Table(name="users_detail")
 * @ORM\Entity(repositoryClass="UserRepositories\User")
 */
class User
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=32, precision=0, scale=0, nullable=false, unique=true)
     */
    private $username;

    /**
     * @var string $fname
     *
     * @ORM\Column(name="fname", type="string", length=32, precision=0, scale=0, nullable=false, unique=false)
     */
    private $fname;

    /**
     * @var string $lname
     *
     * @ORM\Column(name="lname", type="string", length=32, precision=0, scale=0, nullable=false, unique=false)
     */
    private $lname;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, precision=0, scale=0, nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $password;

    /**
     * @var string $createdIp
     *
     * @ORM\Column(name="created_ip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdIp;

    /**
     * @var datetime $dtcreated
     *
     * @ORM\Column(name="dtcreated", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtcreated;

    /**
     * @var integer $active
     *
     * @ORM\Column(name="active", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;

    /**
     * @var string $country
     *
     * @ORM\Column(name="country", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $country;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;

    /**
     * @var string $avatarUrl
     *
     * @ORM\Column(name="avatar_url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $avatarUrl;

    /**
     * @var string $about
     *
     * @ORM\Column(name="about", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $about;

    /**
     * @var integer $logins
     *
     * @ORM\Column(name="logins", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $logins;

    /**
     * @var integer $bans
     *
     * @ORM\Column(name="bans", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bans;

    /**
     * @var datetime $lastActivity
     *
     * @ORM\Column(name="last_activity", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastActivity;

    /**
     * @var date $dob
     *
     * @ORM\Column(name="dob", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dob;

    /**
     * @var integer $sex
     *
     * @ORM\Column(name="sex", type="integer", length=1, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sex;

    /**
     * @var integer $pwReset
     *
     * @ORM\Column(name="pw_reset", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $pwReset;

    /**
     * @var string $apiToken
     *
     * @ORM\Column(name="api_token", type="string", length=32, precision=0, scale=0, nullable=true, unique=false)
     */
    private $apiToken;

    /**
     * @var UserEntities\Permissions
     *
     * @ORM\OneToOne(targetEntity="UserEntities\Permissions", mappedBy="user")
     */
    private $permissions;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ArticlesEntities\Articles", mappedBy="author")
     */
    private $articles;

    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return User
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return User
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdIp
     *
     * @param string $createdIp
     * @return User
     */
    public function setCreatedIp($createdIp)
    {
        $this->createdIp = $createdIp;
        return $this;
    }

    /**
     * Get createdIp
     *
     * @return string 
     */
    public function getCreatedIp()
    {
        return $this->createdIp;
    }

    /**
     * Set dtcreated
     *
     * @param datetime $dtcreated
     * @return User
     */
    public function setDtcreated($dtcreated)
    {
        $this->dtcreated = $dtcreated;
        return $this;
    }

    /**
     * Get dtcreated
     *
     * @return datetime 
     */
    public function getDtcreated()
    {
        return $this->dtcreated;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return User
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set avatarUrl
     *
     * @param string $avatarUrl
     * @return User
     */
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatarUrl = $avatarUrl;
        return $this;
    }

    /**
     * Get avatarUrl
     *
     * @return string 
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * Get about
     *
     * @return string 
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set logins
     *
     * @param integer $logins
     * @return User
     */
    public function setLogins($logins)
    {
        $this->logins = $logins;
        return $this;
    }

    /**
     * Get logins
     *
     * @return integer 
     */
    public function getLogins()
    {
        return $this->logins;
    }

    /**
     * Set bans
     *
     * @param integer $bans
     * @return User
     */
    public function setBans($bans)
    {
        $this->bans = $bans;
        return $this;
    }

    /**
     * Get bans
     *
     * @return integer 
     */
    public function getBans()
    {
        return $this->bans;
    }

    /**
     * Set lastActivity
     *
     * @param datetime $lastActivity
     * @return User
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return datetime 
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * Set dob
     *
     * @param date $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
        return $this;
    }

    /**
     * Get dob
     *
     * @return date 
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set pwReset
     *
     * @param integer $pwReset
     * @return User
     */
    public function setPwReset($pwReset)
    {
        $this->pwReset = $pwReset;
        return $this;
    }

    /**
     * Get pwReset
     *
     * @return integer 
     */
    public function getPwReset()
    {
        return $this->pwReset;
    }

    /**
     * Set apiToken
     *
     * @param string $apiToken
     * @return User
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * Get apiToken
     *
     * @return string 
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * Set permissions
     *
     * @param UserEntities\Permissions $permissions
     * @return User
     */
    public function setPermissions(\UserEntities\Permissions $permissions = null)
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * Get permissions
     *
     * @return UserEntities\Permissions 
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Add articles
     *
     * @param ArticlesEntities\Articles $articles
     * @return User
     */
    public function addArticle(\ArticlesEntities\Articles $articles)
    {
        $this->articles[] = $articles;
        return $this;
    }

    /**
     * Remove articles
     *
     * @param ArticlesEntities\Articles $articles
     */
    public function removeArticle(\ArticlesEntities\Articles $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }
}