<?php

namespace CategoryEntities;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryEntities\Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="CategoryRepositories\Category")
 */
class Category
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string $display
     *
     * @ORM\Column(name="display", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $display;

    /**
     * @var integer $active
     *
     * @ORM\Column(name="active", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;

    /**
     * @var CategoryEntities\Category
     *
     * @ORM\ManyToOne(targetEntity="CategoryEntities\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="related", referencedColumnName="id", nullable=true)
     * })
     */
    private $related;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ArticlesEntities\Articles", mappedBy="category")
     */
    private $articles;

    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set display
     *
     * @param string $display
     * @return Category
     */
    public function setDisplay($display)
    {
        $this->display = $display;
        return $this;
    }

    /**
     * Get display
     *
     * @return string 
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Category
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set related
     *
     * @param CategoryEntities\Category $related
     * @return Category
     */
    public function setRelated(\CategoryEntities\Category $related = null)
    {
        $this->related = $related;
        return $this;
    }

    /**
     * Get related
     *
     * @return CategoryEntities\Category 
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Add articles
     *
     * @param ArticlesEntities\Articles $articles
     * @return Category
     */
    public function addArticle(\ArticlesEntities\Articles $articles)
    {
        $this->articles[] = $articles;
        return $this;
    }

    /**
     * Remove articles
     *
     * @param ArticlesEntities\Articles $articles
     */
    public function removeArticle(\ArticlesEntities\Articles $articles)
    {
        $this->articles->removeElement($articles);
    }

    /**
     * Get articles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }
}