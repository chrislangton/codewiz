<?php

namespace CommentsEntities;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommentsEntities\Comments
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity(repositoryClass="CommentsRepositories\Comments")
 */
class Comments
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", length=11, precision=0, scale=0, nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $disqus_article_id
     *
     * @ORM\Column(name="disqus_article_id", type="integer", length=11, precision=0, scale=0, nullable=false, unique=false)
     */
    private $disqus_article_id;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=45, precision=0, scale=0, nullable=false, unique=false)
     */
    private $type;

    /**
     * @var integer $disqus_id
     *
     * @ORM\Column(name="disqus_id", type="integer", length=11, precision=0, scale=0, nullable=true, unique=true)
     */
    private $disqus_id;

    /**
     * @var datetime $datetime
     *
     * @ORM\Column(name="datetime", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $datetime;

    /**
     * @var integer $approved
     *
     * @ORM\Column(name="approved", type="integer", length=4, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approved;

    /**
     * @var string $content
     *
     * @ORM\Column(name="content", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $content;

    /**
     * @var integer $likes
     *
     * @ORM\Column(name="likes", type="integer", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $likes;

    /**
     * @var integer $dislikes
     *
     * @ORM\Column(name="dislikes", type="integer", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $dislikes;

    /**
     * @var integer $stars
     *
     * @ORM\Column(name="stars", type="integer", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $stars;

    /**
     * @var string $author_name
     *
     * @ORM\Column(name="author_name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $author_name;

    /**
     * @var string $author_disqus_profile
     *
     * @ORM\Column(name="author_disqus_profile", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_disqus_profile;

    /**
     * @var float $author_disqus_reputation
     *
     * @ORM\Column(name="author_disqus_reputation", type="float", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_disqus_reputation;

    /**
     * @var integer $author_disqus_id
     *
     * @ORM\Column(name="author_disqus_id", type="integer", length=11, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_disqus_id;

    /**
     * @var string $author_url
     *
     * @ORM\Column(name="author_url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_url;

    /**
     * @var string $author_about
     *
     * @ORM\Column(name="author_about", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_about;

    /**
     * @var string $author_avatar
     *
     * @ORM\Column(name="author_avatar", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $author_avatar;

    /**
     * @var ArticlesEntities\Articles
     *
     * @ORM\ManyToOne(targetEntity="ArticlesEntities\Articles", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $article;

    /**
     * @var CommentsEntities\Comments
     *
     * @ORM\ManyToOne(targetEntity="CommentsEntities\Comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="in_reply_to", referencedColumnName="disqus_id", nullable=true)
     * })
     */
    private $in_reply_to;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set disqus_article_id
     *
     * @param integer $disqusArticleId
     * @return Comments
     */
    public function setDisqusArticleId($disqusArticleId)
    {
        $this->disqus_article_id = $disqusArticleId;
        return $this;
    }

    /**
     * Get disqus_article_id
     *
     * @return integer 
     */
    public function getDisqusArticleId()
    {
        return $this->disqus_article_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Comments
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set disqus_id
     *
     * @param integer $disqusId
     * @return Comments
     */
    public function setDisqusId($disqusId)
    {
        $this->disqus_id = $disqusId;
        return $this;
    }

    /**
     * Get disqus_id
     *
     * @return integer 
     */
    public function getDisqusId()
    {
        return $this->disqus_id;
    }

    /**
     * Set datetime
     *
     * @param datetime $datetime
     * @return Comments
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * Get datetime
     *
     * @return datetime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set approved
     *
     * @param integer $approved
     * @return Comments
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
        return $this;
    }

    /**
     * Get approved
     *
     * @return integer 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comments
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set likes
     *
     * @param integer $likes
     * @return Comments
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
        return $this;
    }

    /**
     * Get likes
     *
     * @return integer 
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Set dislikes
     *
     * @param integer $dislikes
     * @return Comments
     */
    public function setDislikes($dislikes)
    {
        $this->dislikes = $dislikes;
        return $this;
    }

    /**
     * Get dislikes
     *
     * @return integer 
     */
    public function getDislikes()
    {
        return $this->dislikes;
    }

    /**
     * Set stars
     *
     * @param integer $stars
     * @return Comments
     */
    public function setStars($stars)
    {
        $this->stars = $stars;
        return $this;
    }

    /**
     * Get stars
     *
     * @return integer 
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * Set author_name
     *
     * @param string $authorName
     * @return Comments
     */
    public function setAuthorName($authorName)
    {
        $this->author_name = $authorName;
        return $this;
    }

    /**
     * Get author_name
     *
     * @return string 
     */
    public function getAuthorName()
    {
        return $this->author_name;
    }

    /**
     * Set author_disqus_profile
     *
     * @param string $authorDisqusProfile
     * @return Comments
     */
    public function setAuthorDisqusProfile($authorDisqusProfile)
    {
        $this->author_disqus_profile = $authorDisqusProfile;
        return $this;
    }

    /**
     * Get author_disqus_profile
     *
     * @return string 
     */
    public function getAuthorDisqusProfile()
    {
        return $this->author_disqus_profile;
    }

    /**
     * Set author_disqus_reputation
     *
     * @param float $authorDisqusReputation
     * @return Comments
     */
    public function setAuthorDisqusReputation($authorDisqusReputation)
    {
        $this->author_disqus_reputation = $authorDisqusReputation;
        return $this;
    }

    /**
     * Get author_disqus_reputation
     *
     * @return float 
     */
    public function getAuthorDisqusReputation()
    {
        return $this->author_disqus_reputation;
    }

    /**
     * Set author_disqus_id
     *
     * @param integer $authorDisqusId
     * @return Comments
     */
    public function setAuthorDisqusId($authorDisqusId)
    {
        $this->author_disqus_id = $authorDisqusId;
        return $this;
    }

    /**
     * Get author_disqus_id
     *
     * @return integer 
     */
    public function getAuthorDisqusId()
    {
        return $this->author_disqus_id;
    }

    /**
     * Set author_url
     *
     * @param string $authorUrl
     * @return Comments
     */
    public function setAuthorUrl($authorUrl)
    {
        $this->author_url = $authorUrl;
        return $this;
    }

    /**
     * Get author_url
     *
     * @return string 
     */
    public function getAuthorUrl()
    {
        return $this->author_url;
    }

    /**
     * Set author_about
     *
     * @param string $authorAbout
     * @return Comments
     */
    public function setAuthorAbout($authorAbout)
    {
        $this->author_about = $authorAbout;
        return $this;
    }

    /**
     * Get author_about
     *
     * @return string 
     */
    public function getAuthorAbout()
    {
        return $this->author_about;
    }

    /**
     * Set author_avatar
     *
     * @param string $authorAvatar
     * @return Comments
     */
    public function setAuthorAvatar($authorAvatar)
    {
        $this->author_avatar = $authorAvatar;
        return $this;
    }

    /**
     * Get author_avatar
     *
     * @return string 
     */
    public function getAuthorAvatar()
    {
        return $this->author_avatar;
    }

    /**
     * Set article
     *
     * @param ArticlesEntities\Articles $article
     * @return Comments
     */
    public function setArticle(\ArticlesEntities\Articles $article = null)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * Get article
     *
     * @return ArticlesEntities\Articles 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set in_reply_to
     *
     * @param CommentsEntities\Comments $inReplyTo
     * @return Comments
     */
    public function setInReplyTo(\CommentsEntities\Comments $inReplyTo = null)
    {
        $this->in_reply_to = $inReplyTo;
        return $this;
    }

    /**
     * Get in_reply_to
     *
     * @return CommentsEntities\Comments 
     */
    public function getInReplyTo()
    {
        return $this->in_reply_to;
    }
}