<?php
/**
 * Description of articles
 *
 * @author chris
 */
class Application_Model_Users {
  
  protected $db;

  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    return $this;
  }
  public function getById( $id )
  {
      return $this->db->fetchRow( 'SELECT * FROM users_detail WHERE id=?' , $id );
  }
  public function getByEmail( $email )
  {
      return $this->db->fetchRow( 'SELECT * FROM users_detail WHERE email=?' , $email );
  }
  public function getAll( $limit=100 )
  {
    return $this->db->fetchAssoc( 'SELECT * FROM users_detail LIMIT '.$limit );
  }
  public function insRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false , "category_id" => null );
    $results->result = $this->db->insert( 'category' , $keyPairArr );
    $results->category_id = $this->db->lastInsertId();
    return $results;
  }
  public function updRow( $keyPairArr , $where )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );
    $results->result = $this->db->update( 'users_detail' , $keyPairArr , $where );
    return $results;
  }
  public function delRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );$where=array();
    $where[]=$keyPairArr['id'];
    $where[]=$keyPairArr['name'];
    $where[]=$keyPairArr['display'];
    $results->result = $this->db->query( 'DELETE FROM category WHERE id=? AND name=? AND display=? LIMIT 1' , $where ) ? true : false;
    return $results;
  }
}

?>
