<?php
/**
 * Description of articles
 *
 * @author chris
 */
class Application_Model_Articles {
  protected $db;
  protected $request;
  private $commentsModel;
  private $categoryModel;

  public function __construct( $request = null )
  {
    $this->db = Zend_Registry::get( 'db' );
    $this->request = $request;
    $this->commentsModel = new Application_Model_Comments();
    $this->categoryModel = new Application_Model_Category();
    return $this;
  }
  protected function getUrlParametersByPosition( $pos )
  {
    if ( empty( $this->request ) ) return; else $path = $this->request->getPathInfo();
    $path = explode('/', trim($path, '/'));
    if(@$path[0]== $this->request->getControllerName())
    {
        unset($path[0]);
    }
    if(@$path[1] == $this->request->getActionName())
    {
        unset($path[1]);
    }
    return $path[$pos+1];
  }
  public function viewed($id)
  {
      return $this->db->query( 'UPDATE articles SET views=views+1 WHERE id='.$id );
  }
  public function getRequested()
  {
    $title = $this->getUrlParametersByPosition(1);
    $article = ( empty($title) ? $this->db->fetchRow( "SELECT * FROM `articles` ORDER BY datetime DESC LIMIT 1" ) : $this->db->fetchRow( "SELECT * FROM `articles` WHERE article_title = ? ORDER BY datetime DESC" , array( urldecode( $title ) ) ) );
    $article->authorObj = $this->db->fetchRow( "SELECT * FROM `users_detail` WHERE id = ?" , array( $article->authorid ) );
    if ( is_numeric( $article->category_id ) ) $catResult = $this->categoryModel->fetchById( $article->category_id );
        $article->categoryObj = (object)$catResult[$article->category_id];
    $comments = $this->commentsModel->getByArticle( $article->id );
    $article->numComments = count( $comments );
    return $article;
  }
  public function getById( $id = null )
  {
    if ( !is_numeric( $id ) || is_null( $id ) ) { return (object)array(); }
    $article = $this->db->fetchRow( "SELECT * FROM `articles` WHERE id = ? LIMIT 1" , array( $id ) );
    $article->authorObj = $this->db->fetchRow( "SELECT * FROM `users_detail` WHERE id = ?" , array( $article->authorid ) );
    if ( is_numeric( $article->category_id ) ) $catResult = $this->categoryModel->fetchById( $article->category_id );
        $article->categoryObj = (object)$catResult[$article->category_id];
    $comments = $this->commentsModel->getByArticle( $article->id );
    $article->numComments = count( $comments );
    return $article;
  }
  public function getAll( $limit=100 , $approved = 1 )
  {
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? ORDER BY datetime DESC LIMIT " . (int)$limit , $approved );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function fetchAll( $limit=100 , $order="datetime" , $direction="DESC" )
  {
    $direction = strtoupper($direction) === "DESC" ? "DESC" : "ASC" ;
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` ORDER BY ".$order." ".$direction." LIMIT " . (int)$limit );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function getByTag( $limit=100 , $tag = null )
  {
    if ( is_null( $tag ) ) { $tag = urldecode( $this->getUrlParametersByPosition(1) ); }
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? AND meta LIKE \"%".$tag."%\" ORDER BY datetime DESC LIMIT " . (int)$limit , 1 );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function getByCategory( $limit=100 , $category = null )
  {
    if ( is_null( $category ) ) { $category = urldecode( $this->getUrlParametersByPosition(1) ); }
    $related_arr = array( $category );
    $categories_arr = $this->categoryModel->getByRelated( $category );
    foreach ( $categories_arr as $category ) {
        $related_arr[] = $category['id'];
    }
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = 1 AND category_id IN (" . implode( ',' , $related_arr ) . ") ORDER BY datetime DESC LIMIT " . (int)$limit );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function topTags( $limit = 10 )
  {
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? ORDER BY datetime DESC" , 1 );
    $tags_arr = $tags = array();
    foreach ( $articles as $article ) {
        $tags_arr = explode( ',', $articles[$article['id']]['meta'] );
        foreach ( (array)$tags_arr as $tag ) {
            array_key_exists( strtolower( $tag ), (array)$tags ) ? $tags[strtolower( $tag )]++ : $tags[strtolower( $tag )] = 1 ;
        }
    }
    arsort( $tags ); $i=0; $returnTags = array();
    foreach ( (array)$tags as $key => $value ) {
       $returnTags[$key] = $value; (int)$i++;
       if ( (int)$i >= $limit ) break;
    }
    krsort( $returnTags );
    return (array)$returnTags;
  }
  public function getLatest( $limit = 5 , $exclude = null )
  {
    $articles = is_null( $exclude ) ? 
            $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? ORDER BY datetime DESC LIMIT " . (int)$limit , 1 ):
            $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? AND `id` != ? ORDER BY datetime DESC LIMIT " . (int)$limit , array( 1 , $exclude ) );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function getPopular( $limit = 5 , $exclude = null )
  {
    $articles = is_null( $exclude ) ? 
            $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? ORDER BY views DESC LIMIT " . (int)$limit , 1 ):
            $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE approved = ? AND `id` != ? ORDER BY views DESC LIMIT " . (int)$limit , array( 1 , $exclude ) );
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function getSimilar( $id, $limit = 5 )
  {
    if ( empty( $id ) || !is_numeric( $id ) ) { throw new Exception('Invalid id ' . $id . ' provided to model ' . get_class() . '.') ; return; }
    $article = $this->db->fetchRow( "SELECT * FROM `articles` WHERE approved = ? AND id = ? ORDER BY datetime DESC" , array( 1, $id ) );
    $meta_arr = explode( ",", $article->meta );
    $inTagsSQL = 'AND ((meta LIKE "%' . implode( '%" OR meta LIKE "%' , $meta_arr ) . '%")';
    
    $articles = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE (id != ? AND approved = ?) " . $inTagsSQL . ") ORDER BY views DESC LIMIT " . (int)$limit , array( $id , 1 ) );
    if ( count( $articles ) < $limit ) {
        $notIn = array();
        foreach ( $articles as $article ) {
            $notIn[] = $article['id'];
        }
        $notInSQL = 'AND id NOT IN ("' . implode( '","' , $notIn ) . '")';
        $articles2 = $this->db->fetchAssoc( "SELECT * FROM `articles` WHERE id != ? " . $notInSQL . " AND approved = ? AND category_id = ? ORDER BY views DESC LIMIT " . (int)$limit , array( $id , 1, $article['category_id'] ) );
    }
    if ( isset( $articles2 ) ) $articles = array_merge($articles, $articles2);
    if ( count( $articles ) > $limit ) {
        $stop = 0; $articlesLimit = array();
        foreach ( $articles as $article ) {
            if ( $stop === $limit ) break;
            $articlesLimit[$article['id']] = $article;
            $stop++;
        }
        $articles = $articlesLimit;
    }
    $sql = "SELECT * FROM `users_detail` WHERE id = ?";
    $sql2 = "SELECT * FROM `category` WHERE id = ?";
    foreach ( $articles as $article ) {
        $articles[$article['id']]['authorObj'] = $this->db->fetchRow( $sql , $article['authorid'] );
        $articles[$article['id']]['categoryObj'] = $this->db->fetchRow( $sql2 , $article['category_id'] );
        $comments = $this->commentsModel->getByArticle( $article['id'] );
        $articles[$article['id']]['numComments'] = count( $comments );
    }
    return $articles;
  }
  public function insRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false , "article_id" => null );
    $results->result = $this->db->insert( 'articles' , $keyPairArr );
    $results->article_id = $this->db->lastInsertId();
    return $results;
  }
  public function delRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );
    $rows = $this->db->query( 'DELETE FROM comments WHERE article_id=?' , $keyPairArr['id'] )->rowCount();
    $delComments = is_numeric( $rows ) ? true : false;
    if ($delComments) $results->commentsDeleted = $rows;
    $results->result = $delComments && $this->db->query( 'DELETE FROM articles WHERE id=? LIMIT 1' , $keyPairArr['id'] ) ? true : false;
    return $results;
  }
  public function updRow( $keyPairArr , $where )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );
    $results->result = $this->db->update( 'articles' , $keyPairArr , $where );
    return $results;
  }
}

?>
