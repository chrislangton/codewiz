<?php
/**
 * Description of articles
 *
 * @author chris
 */
class Application_Model_Category {
  
  protected $db;

  public function __construct()
  {
    $this->db = Zend_Registry::get( 'db' );
    return $this;
  }
  public function getAll( $limit=10 , $active = true )
  {
    if ( !is_bool( $active ) ) { $active = true; }
    return $active ?
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? ORDER BY name DESC LIMIT " . (int)$limit , 1 ) :
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? ORDER BY name DESC LIMIT " . (int)$limit , 0 ) ;
  }
  public function fetchAll( $limit=10 , $order="name" , $direction="ASC" )
  {
    $direction = strtoupper($direction) === "ASC" ? "ASC" : "DESC" ;
    return $this->db->fetchAssoc( "SELECT * FROM `category` ORDER BY ".$order." ".$direction." LIMIT " . (int)$limit ) ;
  }
  public function getByName( $limit=10 , $name = null , $active = true )
  {
    if ( is_null( $name ) ) { return array(); }
    if ( !is_bool( $active ) ) { $active = true; }
    return $active ?
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? AND name = ? ORDER BY name DESC LIMIT " . (int)$limit , array( 1 , (string)$name ) ) :
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? AND name = ? ORDER BY name DESC LIMIT " . (int)$limit , array( 0 , (string)$name ) ) ;
  }
  public function getById( $limit=1 , $id = null , $active = true )
  {
    if ( is_null( $id ) ) { return array(); }
    if ( !is_bool( $active ) ) { $active = true; }
    return $active ?
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? AND id = ? ORDER BY id DESC LIMIT " . (int)$limit , array( 1 , (int)$id ) ) :
    $this->db->fetchAssoc( "SELECT * FROM `category` WHERE active = ? AND id = ? ORDER BY id DESC LIMIT " . (int)$limit , array( 0 , (int)$id ) ) ;
  }
  public function fetchById( $id = null )
  {
    if ( is_null( $id ) ) { return array(); }
    return $this->db->fetchAssoc( "SELECT * FROM `category` WHERE id = ? ORDER BY id DESC LIMIT 1" , array( (int)$id ) );
  }
  public function getByRelated( $relatedId = null )
  {
    if ( is_null( $relatedId ) ) { return array(); }
    return $this->db->fetchAssoc( "SELECT * FROM `category` WHERE related = ? ORDER BY name ASC" , array( (int)$relatedId ) );
  }
  public function insRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false , "category_id" => null );
    $results->result = $this->db->insert( 'category' , $keyPairArr );
    $results->category_id = $this->db->lastInsertId();
    return $results;
  }
  public function updRow( $keyPairArr , $where )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );
    $results->result = $this->db->update( 'category' , $keyPairArr , $where );
    return $results;
  }
  public function delRow( $keyPairArr )
  {
    if ( is_null( $keyPairArr ) || !is_array( $keyPairArr ) ) { return false; }
    $results = (object)array( "result" => false );$where=array();
    $where[]=$keyPairArr['id'];
    $where[]=$keyPairArr['name'];
    $where[]=$keyPairArr['display'];
    $results->result = $this->db->query( 'DELETE FROM category WHERE id=? AND name=? AND display=? LIMIT 1' , $where ) ? true : false;
    return $results;
  }
}

?>
