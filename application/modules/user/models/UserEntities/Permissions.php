<?php
namespace UserEntities;

/**
 * UserEntities\Permissions
 *
 * @Table(name="users_permissions")
 * @Entity(repositoryClass="UserRepositories\Permissions")
 * @HasLifecycleCallbacks
 */
class Permissions
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var UserEntities\User
     *
     * @OneToOne(targetEntity="UserEntities\User", inversedBy="permissions")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
            
    /**
     * @var integer $isAdmin
     *
     * @Column(name="admin_user", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAdmin;
            
    /**
     * @var integer $isAuthor
     *
     * @Column(name="create_article", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $isAuthor;
            
    /**
     * @var integer $apiGet
     *
     * @Column(name="api_get", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiGet;
            
    /**
     * @var integer $apiPost
     *
     * @Column(name="api_post", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiPost;
            
    /**
     * @var integer $apiDelete
     *
     * @Column(name="api_delete", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiDelete;
            
    /**
     * @var integer $apiPut
     *
     * @Column(name="api_put", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $apiPut;
            
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param UserRepositories\User $user
     */
    public function setUser( \UserRepositories\User $user )
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return UserEntities\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set isAdmin
     *
     * @param integer $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * Get isAdmin
     *
     * @return integer
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }
    
    /**
     * Set isAuthor
     *
     * @param integer $isAuthor
     */
    public function setIsAuthor($isAuthor)
    {
        $this->isAuthor = $isAuthor;
    }

    /**
     * Get isAuthor
     *
     * @return integer
     */
    public function getIsAuthor()
    {
        return $this->isAuthor;
    }
    
    /**
     * Set apiGet
     *
     * @param integer $apiGet
     */
    public function setApiGet($apiGet)
    {
        $this->apiGet = $apiGet;
    }

    /**
     * Get apiGet
     *
     * @return integer
     */
    public function getApiGet()
    {
        return $this->apiGet;
    }
    
    /**
     * Set apiPost
     *
     * @param integer $apiPost
     */
    public function setApiPost($apiPost)
    {
        $this->apiPost = $apiPost;
    }

    /**
     * Get apiPost
     *
     * @return integer
     */
    public function getApiPost()
    {
        return $this->apiPost;
    }
    
    /**
     * Set apiPut
     *
     * @param integer $apiPut
     */
    public function setApiPut($apiPut)
    {
        $this->apiPut = $apiPut;
    }

    /**
     * Get apiPut
     *
     * @return integer
     */
    public function getApiPut()
    {
        return $this->apiPut;
    }
    
    /**
     * Set apiDelete
     *
     * @param integer $apiDelete
     */
    public function setApiDelete($apiDelete)
    {
        $this->apiPut = $apiDelete;
    }

    /**
     * Get apiDelete
     *
     * @return integer
     */
    public function getApiDelete()
    {
        return $this->apiDelete;
    }
    
}