<?php
namespace UserEntities;

/**
 * UserEntities\User
 *
 * @Table(name="users_detail")
 * @Entity(repositoryClass="UserRepositories\User")
 * @HasLifecycleCallbacks
 */
class User
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var UserEntities\Permissions
     *
     * @OneToOne(targetEntity="UserEntities\Permissions", mappedBy="user")
     */
    private $permissions;
    
    /**
     * @var string $username
     *
     * @Column(name="username", type="string", length=32, precision=0, scale=0, nullable=false, unique=true)
     */
    private $username;
    
    /**
     * @var string $fname
     *
     * @Column(name="fname", type="string", length=32, precision=0, scale=0, nullable=false, unique=false)
     */
    private $fname;
    
    /**
     * @var string $lname
     *
     * @Column(name="lname", type="string", length=32, precision=0, scale=0, nullable=false, unique=false)
     */
    private $lname;
    
    /**
     * @var string $email
     *
     * @Column(name="email", type="string", length=255, precision=0, scale=0, nullable=false, unique=true)
     */
    private $email;
    
    /**
     * @var string $password
     *
     * @Column(name="password", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $password;
    
    /**
     * @var string $createdIp
     *
     * @Column(name="created_ip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdIp;
    
    /**
     * @var datetime $dtcreated
     *
     * @Column(name="dtcreated", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtcreated;
    
    /**
     * @var integer $active
     *
     * @Column(name="active", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;
            
    /**
     * @var string $country
     *
     * @Column(name="country", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $country;
    
    /**
     * @var string $url
     *
     * @Column(name="url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $url;
    
    /**
     * @var string $avatarUrl
     *
     * @Column(name="avatar_url", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $avatarUrl;
    
    /**
     * @var string $about
     *
     * @Column(name="about", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $about;
    
    /**
     * @var integer $logins
     *
     * @Column(name="logins", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $logins;

    /**
     * @var integer $bans
     *
     * @Column(name="bans", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bans;

    /**
     * @var datetime $lastActivity
     *
     * @Column(name="last_activity", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastActivity;
    
    /**
     * @var date $dob
     *
     * @Column(name="dob", type="date", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dob;
    
    /**
     * @var integer $sex
     *
     * @Column(name="sex", type="integer", length=1, precision=0, scale=0, nullable=true, unique=false)
     */
    private $sex;
            
    /**
     * @var integer $pwReset
     *
     * @Column(name="pw_reset", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $pwReset;

    /**
     * @var string $apiToken
     *
     * @Column(name="api_token", type="string", length=32, precision=0, scale=0, nullable=true, unique=false)
     */
    private $apiToken;
            
    /**
     * @var ArticlesEntities\Articles
     *
     * @OneToMany(targetEntity="ArticlesEntities\Articles", mappedBy="author")
     */
    private $articles;
            
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set fname
     * 
     * @param string $fname
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
        return $this;
    }

    /**
     * Get fname
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     * 
     * @param string $lname
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
        return $this;
    }

    /**
     * Get lname
     *
     * @return string
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     * 
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     * 
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdIp
     * 
     * @param string $createdIp
     */
    public function setCreatedIp($createdIp)
    {
        $this->created_ip = $createdIp;
        return $this;
    }

    /**
     * Get createdIp
     *
     * @return string
     */
    public function getCreatedIp()
    {
        return $this->createdIp;
    }

    /**
     * Set dtcreated
     * 
     * @param datetime $dtcreated
     */
    public function setDtcreated($dtcreated)
    {
        $this->dtcreated = $dtcreated;
        return $this;
    }

    /**
     * Get dtcreated
     *
     * @return datetime
     */
    public function getDtcreated()
    {
        return $this->dtcreated;
    }

    /**
     * Set active
     * 
     * @param integer $active
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set country
     * 
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set url
     * 
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set avatarUrl
     * 
     * @param string $avatarUrl
     */
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatarUrl = $avatarUrl;
        return $this;
    }

    /**
     * Get avatarUrl
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * Set about
     * 
     * @param string $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set logins
     * 
     * @param integer $logins
     */
    public function setLogins($logins)
    {
        $this->logins = $logins;
        return $this;
    }

    /**
     * Get logins
     *
     * @return integer
     */
    public function getlogins()
    {
        return $this->logins;
    }

    /**
     * Set bans
     * 
     * @param integer $bans
     */
    public function setBans($bans)
    {
        $this->bans = $bans;
        return $this;
    }

    /**
     * Get bans
     *
     * @return integer
     */
    public function getBans()
    {
        return $this->bans;
    }

    /**
     * Set lastActivity
     * 
     * @param datetime $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return datetime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * Set dob
     * 
     * @param date $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
        return $this;
    }

    /**
     * Get dob
     *
     * @return date
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set sex
     * 
     * @param integer $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * Get sex
     *
     * @return integer
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set pwReset
     * 
     * @param integer $pwReset
     */
    public function setPwReset($pwReset)
    {
        $this->pwReset = $pwReset;
        return $this;
    }

    /**
     * Get pwReset
     *
     * @return integer
     */
    public function getPwReset()
    {
        return $this->pwReset;
    }

    /**
     * Set apiToken
     * 
     * @param string $apiToken
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * Get apiToken
     *
     * @return string
     */
    public function getapiToken()
    {
        return $this->apiToken;
    }
    
    /**
     * Get permissions
     *
     * @return UserEntities\Permissions
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
    
    /**
     * Get articles
     *
     * @return ArticlesEntities\Articles
     */
    public function getArticles()
    {
        return $this->articles;
    }
    
}