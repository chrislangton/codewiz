<?php
namespace CategoryEntities;

/**
 * CategoryEntities\Category
 *
 * @Table(name="category")
 * @Entity(repositoryClass="CategoryRepositories\Category")
 * @HasLifecycleCallbacks
 */
class Category
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;
    
    /**
     * @var string $display
     *
     * @Column(name="display", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $display;
    
    /**
     * @var integer $active
     *
     * @Column(name="active", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $active;

    /**
     * @var CategoryEntities\Category
     *
     * @ManyToOne(targetEntity="CategoryEntities\Category")
     * @JoinColumns({
     *   @JoinColumn(name="related", referencedColumnName="id", nullable=true)
     * })
     */
    private $related;
            
    /**
     * @var ArticlesEntities\Articles
     *
     * @OneToMany(targetEntity="ArticlesEntities\Articles", mappedBy="category")
     */
    private $articles;
            
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set display
     *
     * @param string $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * Get display
     *
     * @return string
     */
    public function getDisplay()
    {
        return $this->display;
    }
    
    /**
     * Set active
     *
     * @param integer $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Set related
     *
     * @param CategoryEntities\Category $related
     */
    public function setRelated( \CategoryEntities\Category $related = null )
    {
        $this->related = $related;
    }

    /**
     * Get related
     *
     * @return CategoryEntities\Category
     */
    public function getRelated()
    {
        return $this->related;
    }
    
    /**
     * Get articles
     *
     * @return ArticlesEntities\Articles
     */
    public function getArticles()
    {
        return $this->articles;
    }
    
}