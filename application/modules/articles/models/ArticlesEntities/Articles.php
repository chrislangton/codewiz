<?php
namespace ArticlesEntities;

/**
 * ArticlesEntities\Articles
 *
 * @Table(name="articles")
 * @Entity(repositoryClass="ArticlesRepositories\Articles")
 * @HasLifecycleCallbacks
 */
class Articles
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", length=12, precision=0, scale=0, nullable=false, unique=true)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string $prevImg
     *
     * @Column(name="prev_img", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $prevImg;
    
    /**
     * @var string $title
     *
     * @Column(name="article_title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $title;
    
    /**
     * @var string $meta
     *
     * @Column(name="meta", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $meta;
    
    /**
     * @var CommentsEntities\Comments
     *
     * @OneToMany(targetEntity="CommentsEntities\Comments", mappedBy="article")
    */
    private $comments;
      
//    /**
//     * @var CategoryEntities\Category
//     *
//     * @ManyToOne(targetEntity="CategoryEntities\Category", inversedBy="articles")
//     * @JoinColumns({
//     *   @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
//     * })
//     */
//    private $category;
    
    /**
     * @var string $content
     *
     * @Column(name="content", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $content;
    
    /**
     * @var datetime $datetime
     *
     * @Column(name="datetime", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $datetime;
    
//    /**
//     * @var UserEntities\User
//     *
//     * @ManyToOne(targetEntity="UserEntities\User", inversedBy="articles")
//     * @JoinColumns({
//     *   @JoinColumn(name="authorid", referencedColumnName="id")
//     * })
//     */
//     private $author;
//    
    /**
     * @var string $createdip
     *
     * @Column(name="createdip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdip;
        
    /**
     * @var integer $approved
     *
     * @Column(name="approved", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approved;
    
    /**
     * @var datetime $dtapproved
     *
     * @Column(name="dtapproved", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dtapproved;
    
//    /**
//     * @var UserEntities\User
//     *
//     * @ManyToOne(targetEntity="UserEntities\User")
//     * @JoinColumns({
//     *   @JoinColumn(name="approvedby", referencedColumnName="id", nullable=true)
//     * })
//     */
//    private $approvedby;
//    
    /**
     * @var integer $approvedby
     *
     * @Column(name="approvedby", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approvedby;
    /**
     * @var string $approvedbyip
     *
     * @Column(name="approvedbyip", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $approvedbyip;
        
    /**
     * @var integer $draft
     *
     * @Column(name="draft", type="integer", length=1, precision=0, scale=0, nullable=false, unique=false)
     */
    private $draft;
    
    /**
     * @var integer $views
     *
     * @Column(name="views", type="integer", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $views;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param integer $active
     */
    public function setActive($approved)
    {
        $this->approved = $approved;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->approved;
    }
    
    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set views
     *
     * @param integer $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }
    
    /**
     * Set draft
     *
     * @param integer $draft
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;
    }

    /**
     * Get draft
     *
     * @return integer 
     */
    public function getDraft()
    {
        return $this->draft;
    }
    
    /**
     * Set approvedbyip
     *
     * @param string $approvedbyip
     */
    public function setApprovedbyip($approvedbyip)
    {
        $this->approvedbyip = $approvedbyip;
    }

    /**
     * Get approvedbyip
     *
     * @return string
     */
    public function getApprovedbyip()
    {
        return $this->approvedbyip;
    }
    
    /**
     * Set approvedby
     *
     * @param UserEntities\User $approvedby
     */
    public function setApprovedby( \UserEntities\User $approvedby )
    {
        $this->approvedby = $approvedby;
    }

    /**
     * Get approvedby
     *
     * @return integer
     */
    public function getApprovedby()
    {
        return $this->approvedby;
    }
    
    /**
     * Set dtapproved
     *
     * @param datetime $dtapproved
     */
    public function setDtapproved($dtapproved)
    {
        $this->dtapproved = $dtapproved;
    }

    /**
     * Get dtapproved
     *
     * @return datetime
     */
    public function getDtapproved()
    {
        return $this->dtapproved;
    }
    
    /**
     * Set author
     *
     * @param UserEntities\User
     */
    public function setAuthor( \UserEntities\User $author)
    {
        $this->author = $author;
    }

    /**
     * Get author
     *
     * @return UserEntities\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
    
    /**
     * Set createdip
     *
     * @param string $createdip
     */
    public function setCreatedip($createdip)
    {
        $this->createdip = $createdip;
    }

    /**
     * Get createdip
     *
     * @return string
     */
    public function getCreatedip()
    {
        return $this->createdip;
    }
    
    /**
     * Set datetime
     *
     * @param datetime $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * Get datetime
     *
     * @return datetime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
    
    /**
     * Set category
     *
     * @param CategoryEntities\Category $category
     */
    public function setCategory( \CategoryEntities\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return CategoryEntities\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Set meta
     *
     * @param string $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }
    
    /**
     * Set prevImg
     *
     * @param string $prevImg
     */
    public function setPrevImg($prevImg)
    {
        $this->prevImg = $prevImg;
    }

    /**
     * Get prevImg
     *
     * @return string
     */
    public function getPrevImg()
    {
        return $this->prevImg;
    }
    
    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get comments
     *
     * @return Comments
     */
    public function getComments()
    {
        return $this->comments;
    }
}