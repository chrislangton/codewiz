<?php
if ($_SERVER['USERDOMAIN'] != 'ROCKINFO') {
    print ('This script should only run locally');
    exit;
}

define('APPLICATION_PATH', __DIR__ . '/..');

set_include_path(
    implode(PATH_SEPARATOR,
	array(
	    realpath(APPLICATION_PATH . '/../library'),
	    get_include_path())
    )
);
// Define application environment to 'development
define('APPLICATION_ENV', 'development');


require 'Zend/Config/Ini.php';
require 'Bootstrap.php';

$bootstrap = Doctrine_Bootstrap::getInstance()->run();

$em = Zend_Registry::get('entity_manager');

$platform = $em->getConnection()->getDatabasePlatform();
$platform->registerDoctrineTypeMapping('enum', 'string');

// Start setting up the CL tool
$helperSet = new \Symfony\Component\Console\Helper\HelperSet(
    array(
	'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
	'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
    )
);

$cli = new \Symfony\Component\Console\Application('Doctrine Command Line Interface', Doctrine\Common\Version::VERSION);

// Display exceptions
$cli->setCatchExceptions(true);
$cli->setHelperSet($helperSet);

// Add all of the desired CL commands
$cli->addCommands(
array(
  new \Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand(),
  new \Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand(),
  new \Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand(),
  new \Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand(),
  new \Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand(),
  new \Doctrine\ORM\Tools\Console\Command\GenerateRepositoriesCommand(),
  new \Doctrine\ORM\Tools\Console\Command\GenerateEntitiesCommand(),
  new \Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand(),
  new \Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand(),
  new \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand(),
  new \Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand(),
  new \Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand()
)
);

// Run the CLI
$cli->run();
