<?php

  require dirname(__FILE__) . '/../../library/Codewiz/Bootstrap.php';
  
  class Doctrine_Bootstrap extends Codewiz_Bootstrap
  {
      
      public static function getInstance()
	{
	    if (null === self::$_instance) {
		self::$_instance = new self();
	    }

	    return self::$_instance;
	}
      
      public function run()
      {
	  
        $this->_setupAutoloader()
                ->_initEnv()
                ->_initDatabase()
                ->_setupDoctrine();

	return $this;
      }
  }