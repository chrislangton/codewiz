<?php
class AdminController extends Codewiz_Controller
{
  private $menuModel;
  private $categoryModel;
  private $articlesModel;
  private $js;

  public function init() {
    $this->menuModel = new Application_Model_Menu();
    $jsPath = APPLICATION_PATH . '/views/scripts/'.$this->getRequest()->getControllerName().'/'.$this->getRequest()->getActionName().'.js';
    $this->js = file_exists($jsPath) ? file_get_contents( $jsPath , FILE_USE_INCLUDE_PATH ) : null ;
    $this->view->session = $this->getSession();
  }
  public function indexAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $request = $this->_request->getParams();
        $this->setMeta();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'sidebar' => $this->menuModel->buildAdminSidebar()
                );
        $this->view->assign( $app_arr );
        if ( isset($request['login']) && $request['login'] )
        {
            $this->raiseAlert( 'Login Successful' , 'success');
        }
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function cronAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $this->setMeta();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'sidebar' => $this->menuModel->buildAdminSidebar(),
                    'jobs' => $this->menuModel->buildCronJobs()
                );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function articleAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $this->categoryModel = new Application_Model_Category();
        $this->articlesModel = new Application_Model_Articles( $this->getRequest() );
        $this->setMeta();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'sidebar' => $this->menuModel->buildAdminSidebar(),
                    'category' => $this->categoryModel->getAll(20),
                    'requested' => (object)array("id"=>null,"content"=>null,"category_id"=>null,"meta"=>null,"prev_img"=>null,"article_title"=>null,"rt_markup"=>null,"show_prev_img"=>null,"banner_img"=>null)
                );
        $request = $this->_request->getParams();
        if ( isset( $request['id'] ) )
        $app_arr['requested'] = $this->articlesModel->getById ( $request['id'] );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( "jQuery('input[name=\"meta\"]').select2({tokenSeparators: [\",\"],tags: ".json_encode( array_keys( $this->articlesModel->topTags() ) )."});" . $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function viewArticlesAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $this->categoryModel = new Application_Model_Category();
        $this->articlesModel = new Application_Model_Articles( $this->getRequest() );
        $this->setMeta();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'articles' => $this->articlesModel->fetchAll(),
                    'sidebar' => $this->menuModel->buildAdminSidebar(),
                    'category' => $this->categoryModel->getAll()
                );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function editCategoriesAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $this->categoryModel = new Application_Model_Category();
        $this->setMeta();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'category' => $this->categoryModel->fetchAll(50),
                    'sidebar' => $this->menuModel->buildAdminSidebar()
                );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function viewUsersAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $users_em = $this->getEm()->getRepository('\UserEntities\User');
        $this->setMeta();
        $app_arr = array(
                    'users' => $users_em->findAll(),
                    'menu' => $this->menuModel->build(),
                    'sidebar' => $this->menuModel->buildAdminSidebar()
                );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
  public function logsAction()
  {
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $this->setMeta();
        $logs = new Application_Model_Logs();
        $app_arr = array(
                    'menu' => $this->menuModel->build(),
                    'sidebar' => $this->menuModel->buildAdminSidebar(),
                    'logs' => $logs->fetchAll()
                );
        $this->view->assign( $app_arr );
        !is_null( $this->js ) ? $this->view->inlineScript()->setScript( $this->js ) : false ;
    }
    else
    {
        $this->_redirect(HOST_URL);
    }
  }
}
