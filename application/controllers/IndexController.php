<?php
class IndexController extends Codewiz_Controller
{
  private $menuModel;
  private $articlesModel;
  private $commentsModel;
  private $request;

  public function init() {
    $this->commentsModel = new Application_Model_Comments();
    $this->articlesModel = new Application_Model_Articles( $this->getRequest() );
    $this->menuModel = new Application_Model_Menu();
    $this->request = $this->getRequest();
    $this->view->session = $this->getSession();
  }
  public function indexAction()
  {
    
//    $head = new Codewiz_HeadSection( array("title"=>"test","description"=>"more details") , array("title"=>"test","description"=>"more details") );
//    var_dump( $head->meta("test","test2") );
    
    if ( isset( $this->request->article ) )
    {
        $this->_redirect(HOST_URL."article/post/" . $this->request->article );
    }
    $this->setMeta();
    $articles = $this->articlesModel->getAll(15);
    $app_arr = array(
                'menu' => $this->menuModel->build(),
                'articles' => $articles,
                'recentComments' => $this->commentsModel->getLatest( 10 ),
                'popularArticles' => $this->articlesModel->getPopular( 10 ),
                'tags' => $this->articlesModel->topTags( 20 )
            );
    $this->view->assign( $app_arr );
  }
  public function tagAction()
  {
    $this->setMeta();
    $articles = $this->articlesModel->getByTag(15);
    $app_arr = array(
                'menu' => $this->menuModel->build(),
                'articles' => $articles,
                'recentComments' => $this->commentsModel->getLatest(),
                'popularArticles' => $this->articlesModel->getPopular( 15 ),
                'tags' => $this->articlesModel->topTags( 20 )
            );
    $this->view->assign( $app_arr );
    $this->_helper->viewRenderer('index');
  }
  public function categoryAction()
  {
    $this->setMeta();
    $articles = $this->articlesModel->getByCategory(15);
    $app_arr = array(
                'menu' => $this->menuModel->build(),
                'articles' => $articles,
                'recentComments' => $this->commentsModel->getLatest(),
                'popularArticles' => $this->articlesModel->getPopular( 15 ),
                'tags' => $this->articlesModel->topTags( 20 )
            );
    $this->view->assign( $app_arr );
    $this->_helper->viewRenderer('index');
  }
}
