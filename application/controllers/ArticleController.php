<?php
class ArticleController extends Codewiz_Controller
{
  private $menuModel;
  private $articlesModel;
  private $commentsModel;
  
  public function init() {
    $this->commentsModel = new Application_Model_Comments();
    $this->articlesModel = new Application_Model_Articles( $this->getRequest() );
    $this->menuModel = new Application_Model_Menu();
    $this->view->session = $this->getSession();
  }
  public function postAction()
  {
    $this->setMeta();
    $article = $this->articlesModel->getRequested();
    $app_arr = array(
                'menu' => $this->menuModel->build(),
                'similarArticles' => $this->articlesModel->getSimilar( $article->id , 10 ),
                'recentComments' => $this->commentsModel->getLatest( 10 , $article->id ),
                'popularArticles' => $this->articlesModel->getPopular( 10 , $article->id ),
                'latestArticles' => $this->articlesModel->getLatest( 10 , $article->id ),
                'article' => $article,
                'tw_type' => isset( $article->banner_img ) && !empty( $article->banner_img ) ? "summary_large_image" : "summary" ,
//                'tw_type' => $this->curl_get_file_headers( $img )->content_length < 25000 ? "summary" : isset( $article->banner_img ) && !empty( $article->banner_img ) ? "photo" : "summary_large_image" ,
                'keywords' => $article->meta ,
                'article_title' => $article->article_title,
                'abstract' => $article->article_title,
                'image_src' => isset( $article->banner_img ) && !empty( $article->banner_img ) ? $article->banner_img : $article->prev_img,
                'description' => $this->trim_words( $article->content , 200 , true )
            );
    $this->view->assign( $app_arr );
    $this->articlesModel->viewed( $article->id );
  }
}
