<?php
class ApiController extends Codewiz_Controller
{
    // blowfish
    private static $algo = '$2a';
    // cost parameter
    private static $cost = '$10';
    private $_rawData;
    private $usersModel;
    private $articlesModel;
    private $categoryModel;

    public function init() {
        $list = explode( '/', $this->_request->getRawBody() );
        $result = array();
        if ( is_array( $list ) && isset( $list[1] ) )
        for ($i=0 ; $i<count($list) ; $i+=2) {
            $result[ rawurldecode( $list[$i] ) ] = rawurldecode( $list[$i+1] );
        }
        $this->_rawData = (object)$result;
        $this->articlesModel = new Application_Model_Articles( $this->getRequest() );
        $this->categoryModel = new Application_Model_Category();
        $this->usersModel = new Application_Model_Users();
    }
    public function loginAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $request = (object)$this->_request->getParams();
        exit( json_encode( $this->verifyLogin( $request , $this->usersModel->getByEmail( $request->email ) ) ) );
      return;
    }
    public function logoutAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $request = (object)$this->_request->getParams();
        $result = (object)array( "auth" => false , "err" => "" );
        if ( !isset( $request->id ) )
        {
            $result->err .= 'User id was not set, are you logged in?'.PHP_EOL;
        }
        unset(  $this->getSession()->email ,
                $this->getSession()->userId ,
                $this->getSession()->url ,
                $this->getSession()->avatar ,
                $this->getSession()->userName );
        $this->getSession()->auth = false;
        $result->auth = false;

        echo json_encode( $result ) ;

      return;
    }
    public function createArticleAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $input['meta'] = strtolower( implode( ',' , $input['meta'] ) );
                $input['authorid'] = $this->getSession()->userId;
                $input['createdip'] = $_SERVER['REMOTE_ADDR'];
                $input['approved'] = "0";
                //$input['draft'] = "1"; //this is for prod
                foreach ( $input as $key => $value )
                {
                    if (is_numeric( $key ) )
                    unset( $input[$key] );
                }
                $results = array_merge( (array)$results, (array)$this->articlesModel->insRow( $input ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function deleteArticleAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->id = $input['id'];
                $results->title = $input['article_title'];
                unset( $input['null'] );
                unset( $input['content'] );
                $results = array_merge( (array)$results, (array)$this->articlesModel->delRow( $input ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function updateArticleAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->article_id = $input['id'];
                unset( $input['null'] );
                unset( $input['id'] );
                unset( $input['createdip'] );
                unset( $input['authorid'] );
                unset( $input['datetime'] );
                if (isset($input['meta']) && is_array($input['meta'])) $input['meta'] = strtolower( implode( ',' , $input['meta'] ) );
                foreach ( $input as $key => $value )
                {
                    if (is_numeric( $key ) )
                    unset( $input[$key] );
                }
                $results = array_merge( (array)$results, (array)$this->articlesModel->updRow( $input , array("id=".$results->article_id ) ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function updateCategoryAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->category_id = $input['id'];
                if ( $input['related'] == 0 ) $input['related'] = null;
                unset( $input['null'] );
                unset( $input['id'] );
                $results = array_merge( (array)$results, (array)$this->categoryModel->updRow( $input , array("id=".$results->category_id ) ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function createCategoryAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                if ( $input['related'] == 0 ) $input['related'] = null;
                unset( $input['null'] );
                unset( $input['id'] );
                $results = array_merge( (array)$results, (array)$this->categoryModel->insRow( $input ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function deleteCategoryAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->id = $input['id'];
                $results->display = $input['display'];
                unset( $input['null'] );
                $results = array_merge( (array)$results, (array)$this->categoryModel->delRow( $input ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function updateUserAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            } 
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->user_id = $input['id'];
                $results->username = $input['username'];
                unset( $input['null'] );
                unset( $input['id'] );
                unset( $input['username'] );
                unset( $input['bans'] );
                unset( $input['api_token'] );
                $name_arr = explode( " " , $input['name'] );
                $input['fname'] = $name_arr[0];
                unset($name_arr[0]);
                $input['lname'] = implode( " " , $name_arr );
                $input['last_activity'] = date("Y-m-d H:i:s");
                unset($input['name']);
                $results = array_merge( (array)$results, (array)$this->usersModel->updRow( $input , array("id=".$results->user_id ) ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function createUserAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
//        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
//        {
//            $results->auth = true; $results->err = "";
//            if ( !isset( $this->_rawData->input ) )
//            {
//                $results->err .= 'Missing required data'.PHP_EOL;
//            } 
//            else
//            {
//                $input = json_decode( $this->_rawData->input , true );
//                if ( $input['related'] == 0 ) $input['related'] = null;
//                unset( $input['null'] );
//                unset( $input['id'] );
//                $results = array_merge( (array)$results, (array)$this->categoryModel->insRow( $input ) );
//            }            
//        }
        echo json_encode( $results ) ;
      return;
    }
    public function deleteUserAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
//        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
//        {
//            $results->auth = true; $results->err = "";
//            if ( !isset( $this->_rawData->input ) )
//            {
//                $results->err .= 'Missing required data'.PHP_EOL;
//            } 
//            else
//            {
//                $input = json_decode( $this->_rawData->input , true );
//                $results->id = $input['id'];
//                $results->display = $input['display'];
//                unset( $input['null'] );
//                $results = array_merge( (array)$results, (array)$this->categoryModel->delRow( $input ) );
//            }            
//        }
        echo json_encode( $results ) ;
      return;
    }
    public function generateTokenAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
//        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
//        {
//            $results->auth = true; $results->err = "";
//            if ( !isset( $this->_rawData->input ) )
//            {
//                $results->err .= 'Missing required data'.PHP_EOL;
//            } 
//            else
//            {
//                $input = json_decode( $this->_rawData->input , true );
//                $results->id = $input['id'];
//                $results->display = $input['display'];
//                unset( $input['null'] );
//                $results = array_merge( (array)$results, (array)$this->categoryModel->delRow( $input ) );
//            }            
//        }
        echo json_encode( $results ) ;
      return;
    }
    public function resetPasswordAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
//        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
//        {
//            $results->auth = true; $results->err = "";
//            if ( !isset( $this->_rawData->input ) )
//            {
//                $results->err .= 'Missing required data'.PHP_EOL;
//            } 
//            else
//            {
//                $input = json_decode( $this->_rawData->input , true );
//                $results->id = $input['id'];
//                $results->display = $input['display'];
//                unset( $input['null'] );
//                $results = array_merge( (array)$results, (array)$this->categoryModel->delRow( $input ) );
//            }            
//        }
        echo json_encode( $results ) ;
      return;
    }
    public function deleteLogAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $logsModel = new Application_Model_Logs();
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            if ( !isset( $this->_rawData->input ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            }
            else
            {
                $input = json_decode( $this->_rawData->input , true );
                $results->id = $input['id'];
                unset( $input['null'] );
                $results = array_merge( (array)$results, (array)$logsModel->delRowById( $input['id'] ) );
            }            
        }
        echo json_encode( $results ) ;
      return;
    }
    public function deleteLogsAction()
    {
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $logsModel = new Application_Model_Logs();
        $results = (object)array( "auth" => false , "err" => "not authenticated" , "result" => false );
        if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
        {
            $results->auth = true; $results->err = "";
            $input = $this->_request->getParams();
            if ( !isset( $input['logs'] ) )
            {
                $results->err .= 'Missing required data'.PHP_EOL;
            }
            else
            {
                $results->ids = $input['logs'];
                foreach( $input['logs'] as $logId )
                $results = array_merge( (array)$results, (array)$logsModel->delRowById( $logId ) );
            }            
        }
        echo json_encode( $results ) ;
        $this->_redirect( HOST_URL . 'admin/logs/' );
      return;
    }
}
