<?php
require_once 'Disqus/savePostsToDb.php';

class CronController extends Codewiz_Controller
{
  private $_usersModel;
  
  public function indexAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_redirect(HOST_URL."admin/cron/");
  }
  public function disqusallAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "inserted" => 0 );
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $savePostsToDb = new savePostsToDb();
        $results->inserted = $savePostsToDb->runAll();
    }
    exit( json_encode( $results ) ) ;
  }
  public function disquslatestAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "inserted" => 0 );
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $savePostsToDb = new savePostsToDb();
        $results->inserted = $savePostsToDb->runLatest();
    }
    exit( json_encode( $results ) ) ;
  }
  public function disqusnewAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "inserted" => 0 );
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $savePostsToDb = new savePostsToDb();
        $results->inserted = $savePostsToDb->runNew();
    }
    exit( json_encode( $results ) ) ;
  }
  public function disqusupdateAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "updated" => 0 );
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $savePostsToDb = new savePostsToDb();
        $results->updated = $savePostsToDb->runUpdate();
    }
    exit( json_encode( $results ) ) ;
  }
  public function generateSitemapAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "result" => 0 );
    $results->file = "sitemap";
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $sitemapName = PUBLIC_PATH . '/sitemap.xml';
        if ( file_exists( $sitemapName ) )
        {
            unlink( $sitemapName );
        }
        $handle = fopen( $sitemapName , 'w');
        $xml = '';
        $xml .='<?xml version="1.0" encoding="UTF-8"?>';
           /* $xml .='<?xml-stylesheet type="text/xsl" href="' . HOST_URL . 'sitemap-stylesheet.xsl"?>'; */
            $xml .='<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; // xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
            $xml .= '
                <url>
                <loc>' . HOST_URL . '</loc>
                <lastmod>' . date("Y-m-d") . '</lastmod>
                <changefreq>hourly</changefreq>
                <priority>1.0</priority>
                </url>
                ';
        $articlesModel = new Application_Model_Articles();
        foreach ( $articlesModel->getAll() as $article ) {
            $xml .='<url>
                    <loc>' . HOST_URL . 'article/post/' . strtolower( rawurlencode( $article['article_title'] ) ) . '</loc>';
//            $xml .='<news:news>
//                    <news:publication>
//                    <news:name>' . $this->getConf()->appName . '</news:name>
//                    <news:language>en</news:language>
//                    </news:publication>
//                    <news:genres>Blog</news:genres>
//                    <news:publication_date>' . date( "Y-m-d", strtotime( $article['datetime'] ) ) . '</news:publication_date>
//                    <news:title>' . utf8_encode( $this->xmlentities( $article['article_title'] ) ) . '</news:title>
//                    <news:keywords>' . utf8_encode( $this->xmlentities( $article['meta'] ) ) . '</news:keywords>
//                    </news:news>';
            $xml .='<lastmod>' . date( "Y-m-d", strtotime( $article['datetime'] ) ) . '</lastmod>
                    <changefreq>weekly</changefreq>
                    <priority>0.8</priority>
                    </url>';
        }
        $xml .= '</urlset>';

        $results->result = fwrite( $handle, $xml );
        fclose( $handle );
//    header('Content-Type: text/xml;charset=utf-8');
//    header('Content-Disposition: attachment; filename="sitemap.xml"');
//    echo $xml;
    }
    exit( json_encode( $results ) ) ;
  }
  public function pingSitemapAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    
    echo json_encode( PUBLIC_PATH ) ;
  }
  public function generateRssAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    $this->_usersModel = new Application_Model_Users();
    $request = (object)$this->_request->getParams();
    $results = !isset( $this->getSession()->auth ) && !$this->getSession()->auth ?
            $this->verifyLogin( $request , $this->_usersModel->getByEmail( $request->email ) ):
            (object)array( "auth" => false , "err" => "not authenticated" , "result" => 0 );
    $results->file = "rss";
    
    if ( isset( $this->getSession()->auth ) && $this->getSession()->auth )
    {
        $results->auth = true; $results->err = "";
        $sitemapName = PUBLIC_PATH . '/rss.xml';
        if ( file_exists( $sitemapName ) )
        {
            unlink( $sitemapName );
        }
        $handle = fopen( $sitemapName , 'w');
        $xml = '';
        $xml .='<?xml version="1.0" encoding="UTF-8"?>
                <rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
                <channel>
                <atom:link href="' . HOST_URL . 'rss.xml" rel="self" type="application/rss+xml" />
                <title>' . utf8_encode($this->xmlentities( $this->getConf()->appName )) . '</title>
                <link>' . HOST_URL . '</link>
                <description>' . utf8_encode($this->xmlentities( $this->getConf()->description )) . '</description>
                <language>en</language>
                <copyright>' . utf8_encode($this->xmlentities( date('Y') . " " . $this->getConf()->appName )) . '</copyright>
                <pubDate>' . date( "Y-m-d" ) . '</pubDate>
                <lastBuildDate>' . date('r') . '</lastBuildDate>
                <managingEditor>' . utf8_encode($this->xmlentities( $this->getConf()->author )) . ' (' . utf8_encode($this->xmlentities( $this->getConf()->adminEmail )) . ')</managingEditor>
                <webMaster>' . utf8_encode($this->xmlentities( $this->getConf()->author )) . ' (' . utf8_encode($this->xmlentities( $this->getConf()->adminEmail )) . ')</webMaster>
                <generator>' . utf8_encode($this->xmlentities( $this->getConf()->appName )) . '</generator>
                <docs>http://cyber.law.harvard.edu/rss/rss.html</docs>
                <ttl>60</ttl>
                <image>
                <url>' . HOST_URL . 'img/logo-128.png</url>
                <title>' . utf8_encode($this->xmlentities( $this->getConf()->appName )) . '</title>
                <link>' . HOST_URL . '</link>
                <description>' . utf8_encode($this->xmlentities( $this->getConf()->shortDescription )) . '</description>
                <width>128</width>
                <height>128</height>
                </image>';
        $articlesModel = new Application_Model_Articles();
        foreach ( $articlesModel->getAll(10) as $article ) {
            $xml .='<item>
                    <title>' . utf8_encode($this->xmlentities($article['article_title'])) . '</title>
                    <link>' . HOST_URL . 'article/post/' . strtolower(rawurlencode($article['article_title'])) . '</link>
                    <description><![CDATA[' . utf8_encode($this->xmlentities($this->trim_words($article['content'],500,true)))  . ']]></description>
                    <content:encoded><![CDATA[' . utf8_encode($this->xmlentities($article['content'])) . ']]></content:encoded>
                    <dc:creator>' . utf8_encode($this->xmlentities($article['authorObj']->fname)) . ' ' . utf8_encode($this->xmlentities($article['authorObj']->lname)) . '</dc:creator>
                    <category>' . utf8_encode($this->xmlentities($article['categoryObj']->display)) . '</category>
                    <guid>' . HOST_URL . 'article/post/' . strtolower(rawurlencode($article['article_title'])) . '</guid>
                    <comments>' . HOST_URL . 'article/post/' . strtolower(rawurlencode($article['article_title'])) . '</comments>
                    <slash:comments>' . utf8_encode($this->xmlentities($article['numComments'])) . '</slash:comments>
                    <pubDate>' . date('r',strtotime($article['datetime'])) . '</pubDate>
                    <source url="' . HOST_URL . 'rss.xml">' . utf8_encode($this->xmlentities( $this->getConf()->appName )) . ' RSS Feed</source>
                    <enclosure length="' . $this->curl_get_file_headers( $article['prev_img'] )->content_length . '" type="' . $this->curl_get_file_headers( $article['prev_img'] )->content_type . '" url="' . utf8_encode($this->xmlentities( $article['prev_img'] ) ) . '" />
                    </item>';
        }
        $xml .='</channel>
                </rss>';

        $results->result = fwrite( $handle, $xml );
        fclose( $handle );
//    header('Content-Type: text/xml;charset=utf-8');
//    header('Content-Disposition: attachment; filename="rss.xml"');
//    echo $xml;
    }
    exit( json_encode( $results ) ) ;
  }
  public function pingRssAction()
  {
    $this->_helper->viewRenderer->setNoRender(TRUE);
    
    exit( json_encode( $results ) ) ;
  }
}
