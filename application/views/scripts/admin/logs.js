function a(name,id) {
    jQuery('['+name+'="'+id+'"]').toggle();
}
function delRow(id)
{
    $json({
        data: {
            input:      JSON.stringify( {id:id} )
        },
        method:'DELETE',
        url: location.protocol+'\/\/'+location.host+'/api/delete-log/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Log Deleted: ['+json.id+']'});
                $('tr[log_id=\"'+json.id+'\"]').addClass('error').fadeOut(1000,function(){$('tr[log_id=\"'+json.id+'\"]').remove();});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}