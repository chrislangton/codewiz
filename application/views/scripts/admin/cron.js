function cronRun( url , resultAttr )
{
    $json({
        data:{},
        method:'GET',
        url: url,
        success: function( json ) {
            $('[result=\"'+resultAttr+'\"]').html('');
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                if ( typeof json.inserted !== 'undefined' )
                $('[result=\"'+resultAttr+'\"]').html('Inserted '+json.inserted+' Rows');
                if ( typeof json.updated !== 'undefined' )
                $('[result=\"'+resultAttr+'\"]').html('Updated '+json.updated+' Rows');
                $('[result=\"'+resultAttr+'\"]').attr('class','').addClass('label-success');
                if ( typeof json.result !== 'undefined' & json.result > 0 )
                $('[result=\"'+resultAttr+'\"]').html('<a class="muted" href=\"'+location.protocol+'\/\/'+location.host+'/'+json.file+'.xml\">XML File</a> Generated.');
                $('[result=\"'+resultAttr+'\"]').attr('class','').addClass('label-success');
            }
            if (typeof(json.err) !== 'undefined' && json.err !== '')
            {
                $('[result=\"'+resultAttr+'\"]').append(json.err);
                $('[result=\"'+resultAttr+'\"]').attr('class','').addClass('label-warning');
            }
        },
        error: function( json )
        {
            $('[result=\"'+resultAttr+'\"]').html('Server Error');
            $('[result=\"'+resultAttr+'\"]').attr('class','').addClass('label-warning');
        }
    });
}