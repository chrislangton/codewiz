var NewArticleView = {
    view: 'new_article',
    json: '[]',
    save_new_article: function(input,ele,data,btn,e){
        input.content = CKEDITOR.instances.content.getData();
        input.meta = jQuery('input[name=\"meta\"]').select2('val');
        input.show_prev_img = jQuery('input[name=\"show_prev_img\"]').is(':checked');
        if ( input.article_title === '' ) {
            $('#alert').alert({type:'warning',txt:'Title must not be blank'}); return;
        }
        if ( input.prev_img === '' ) {
            $('#alert').alert({type:'warning',txt:'Image Url must not be blank'}); return;
        }
        $json({
            data: {
                input:      JSON.stringify( input )
            },
            method:'POST',
            url: location.protocol+'\/\/'+location.host+'/api/create-article/',
            success: function( json ) {
                if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
                {
                    $('#alert').alert({type:'success',txt:'New Article Id: '+json.article_id+' <a href=\"'+location.protocol+'\/\/'+location.host+'/admin/view-articles/\">Approve</a>'});
                }
                else 
                {
                    typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
                }
            },
            error: function( json )
            {
                $('#alert').alert({type:'error',txt:'Server Error'});
            }
        });
    },
    update_article: function(input,ele,data,btn,e){
        input.content = CKEDITOR.instances.content.getData();
        input.meta = jQuery('input[name=\"meta\"]').select2('val');
        input.show_prev_img = jQuery('input[name=\"show_prev_img\"]').is(':checked');
        if ( input.article_title === '' ) {
            $('#alert').alert({type:'warning',txt:'Title must not be blank'}); return;
        }
        if ( input.prev_img === '' ) {
            $('#alert').alert({type:'warning',txt:'Image Url must not be blank'}); return;
        }
        $json({
            data: {
                input:      JSON.stringify( input )
            },
            method:'POST',
            url: location.protocol+'\/\/'+location.host+'/api/update-article/',
            success: function( json ) {
                if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
                {
                    $('#alert').alert({type:'success',txt:'New Article Id: '+json.article_id+' <a href=\"'+location.protocol+'\/\/'+location.host+'/admin/view-articles/\">Approve</a>'});
                }
                else 
                {
                    typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
                }
            },
            error: function( json )
            {
                $('#alert').alert({type:'error',txt:'Server Error'});
            }
        });
    }
};
$CreateViewBindings(NewArticleView);
CKEDITOR.disableAutoInline = true; CKEDITOR.replace( 'content' );
