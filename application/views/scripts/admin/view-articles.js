function saveRow(id)
{
    var values = {};
    $('tr[article_id=\"'+id+'\"] td *').each(function(input){
      values[$(input).attr('name')] = $(input).val();
    });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/update-article/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Article '+json.article_id+' Updated'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function delRow(id)
{
    var values = {};
    $('tr[article_id=\"'+id+'\"] td *').each(function(input){
      values[$(input).attr('name')] = $(input).val();
    });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/delete-article/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Article Deleted: ['+json.id+'] '+json.title+'. '+json.commentsDeleted+' Comments purged'});
                $('tr[article_id=\"'+json.id+'\"]').addClass('error').fadeOut(1000,function(){$('tr[article_id=\"'+json.id+'\"]').remove();});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}