function saveRow(id)
{
    var values = {};
    $('tr[user_id=\"'+id+'\"] td *').each(function(input){
      values[$(input).attr('name')] = $(input).val();
    });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'PUT',
        url: location.protocol+'\/\/'+location.host+'/api/update-user/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'User ['+json.user_id+'] '+json.username+' Updated'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function delRow(id)
{
    var values = {};
    $('tr[user_id=\"'+id+'\"] td *').each(function(input){
      values[$(input).attr('name')] = $(input).val();
    });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'DELETE',
        url: location.protocol+'\/\/'+location.host+'/api/delete-user/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'User Deleted: ['+json.user_id+'] '+json.username+'. '+json.articlesEffected+' Articles Moved to Admin'});
                $('tr[user_id=\"'+json.user_id+'\"]').addClass('error').fadeOut(1000,function(){$('tr[user_id=\"'+json.user_id+'\"]').remove();});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function createRow()
{
  var values = {};
  $('#new_user *').each(function(input){
    values[$(input).attr('name')] = $(input).val();
  });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/create-user/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'User '+json.user_id+' Created'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function genApiToken(id)
{
  var values = { id:     id };
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/generate-token/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'API Token Created'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function resetPw(id)
{
  var values = { id:     id };
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/reset-password/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Password Reset'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}