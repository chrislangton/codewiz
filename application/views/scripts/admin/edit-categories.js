function delRow(id)
{
    var values = {};
    $('tr[category_id=\"'+id+'\"] td *').each(function(input){
      values[$(input).attr('name')] = $(input).val();
    });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'DELETE',
        url: location.protocol+'\/\/'+location.host+'/api/delete-category/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Category ['+json.id+'] '+json.display+' Deleted'});
                $('tr[category_id=\"'+json.id+'\"]').addClass('error').fadeOut(1000,function(){$('tr[category_id=\"'+json.id+'\"]').remove();});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function saveRow(id)
{
  var values = {};
  $('tr[category_id=\"'+id+'\"] td *').each(function(input){
    values[$(input).attr('name')] = $(input).val();
  });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'PUT',
        url: location.protocol+'\/\/'+location.host+'/api/update-category/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Category '+json.category_id+' Updated'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}
function createRow()
{
  var values = {};
  $('#new_category *').each(function(input){
    values[$(input).attr('name')] = $(input).val();
  });
    $json({
        data: {
            input:      JSON.stringify( values )
        },
        method:'POST',
        url: location.protocol+'\/\/'+location.host+'/api/create-category/',
        success: function( json ) {
            if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
            {
                $('#alert').alert({type:'success',txt:'Article '+json.category_id+' Created'});
            }
            else 
            {
                typeof(json.err) !== 'undefined' && json.err !== '' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
            }
        },
        error: function( json )
        {
            $('#alert').alert({type:'error',txt:'Server Error'});
        }
    });
}