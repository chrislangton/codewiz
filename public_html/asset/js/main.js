if ( codewiz.auth ) {
    $('button#signout').on('click',function(){
        $json({
            data: { id: codewiz.userId },
            url: location.protocol+"\/\/"+location.host+"/api/logout/",
            success: function( json ) {
                if ( typeof( json.auth ) !== 'undefined' && json.auth === false )
                {
                    $('#alert').alert({type:'success',txt:'Logged out'});
                    $('div#login').removeClass("hide");
                    $('button#signout').addClass("hide");
                    codewiz.auth = false;
                }
                else 
                {
                    typeof(json.err) !== 'undefined' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
                }
            },
            error: function( json )
            {
                $('#alert').alert({type:'error',txt:'Server Error'});
            }
	});        
    });
}
var LoginFormView = {
    view: 'login_form',
    json: '["none"]',
    observe: { 
        email: function(ele,e){
            if ( $(ele).val().length < 6 ) {
                $('#alert').alert({txt:'Email to short to be valid.',type:'warning'});
                return;
            }
            if ( $(ele).val().length > 254 ) {
                $('#alert').alert({txt:'Email exceeds allowable character length (255).',type:'warning'});
                return;
            }
        },
        password: function(ele,e){
            if ( $(ele).val().length < 8 ) {
                $('#alert').alert({txt:'Please use at least 8 characters.',type:'warning'});
                return;
            }
            if ( $(ele).val().length > 254 ) {
                $('#alert').alert({txt:'Password exceeds allowable character length (255).',type:'warning'});
                return;
            }
        }
    },
    loginFn: function(input,ele,data,btn,e){
        if ( input.email.length < 8 || input.password.length < 8 ) {
            $('#alert').alert({txt:'Please use at least 8 characters.',type:'error'});
            return;
        }
        if ( input.email.length > 254 || input.password.length > 254 ) {
            $('#alert').alert({txt:'Input exceeds allowable character length (255).',type:'error'});
            return;
        }
        if ( input.email.validEmail() !== true ) {
            $('#alert').alert({txt:'Email format invalid.',type:'error'});
            return;
        }
        $json({
            data: {email:input.email,password:input.password},
            url: location.protocol+"\/\/"+location.host+"/api/login/",
            success: function( json ) {
                if ( typeof( json.auth ) !== 'undefined' && json.auth === true )
                {
                   window.location = location.protocol+"\/\/"+location.host+"/admin/?login=true";
                }
                else 
                {
                    typeof(json.err) !== 'undefined' ? $('#alert').alert({type:'error',txt:json.err}) : $('#alert').alert({type:'error',txt:'Server Error'});
                }
            },
            error: function( json )
            {
                $('#alert').alert({type:'error',txt:'Server Error'});
            }
	});
    }
};
$CreateViewBindings(LoginFormView);
$json({
    Start: function(){
        $('#ajax').show();
    },
    End: function(){
        $('#ajax').hide();
    }
});