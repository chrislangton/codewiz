<?php
// This is the bootstrap file.
// Define domain url and protocol
defined( 'APP_VERSION' )
    || define( 'APP_VERSION', '20120426' );
// Define domain url and protocol
defined( 'HOST_URL' )
    || define( 'HOST_URL', stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' . basename( $_SERVER['HTTP_HOST'] ) . '/' : 'http://'.basename( $_SERVER['HTTP_HOST'] )  . '/' );
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
// Define path to public directory
defined('PUBLIC_PATH')
    || define('PUBLIC_PATH', realpath(dirname(__FILE__) . '/../public_html'));
// Define application environment const APPLICATION_ENV
    //apache_setenv('APPLICATION_ENV', 'development');
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
// add library/ directory to the include_path, it contains ZF install
set_include_path(implode(PATH_SEPARATOR, array(
    dirname(dirname(__FILE__)) . '/library',
    get_include_path(),
)));
set_include_path(implode(PATH_SEPARATOR, array(
    dirname(dirname(__FILE__)) . '/application/models',
    get_include_path(),
)));
if ( "production" !== APPLICATION_ENV )
    error_reporting(-1);

// Application
require_once 'Codewiz/Bootstrap.php';
// Instantiate and dispatch the front controller. It will route request to action controllers.
Codewiz_Bootstrap::getInstance()
                    ->run();
